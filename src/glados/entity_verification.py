"""
Module with decorators that help with the redirection and verification of ids
"""
import requests
from functools import wraps

from django.conf import settings
from django.urls import reverse
from django.http import HttpResponseRedirect

ENTITY_NAME_TO_URL_NAME = {
    'COMPOUND': 'compound_report_card',
    'TARGET': 'target_report_card',
    'ASSAY': 'assay_report_card',
    'DOCUMENT': 'document_report_card',
    'CELL': 'cell_line_report_card',
    'TISSUE': 'tissue_report_card'
}


def verify_entity(desired_entity_type):
    """
    :param desired_entity_type: the desired entity type
    :return: a generated decorator with the desired entity
    """

    def wrap(func):
        """
        :param func: function to decorate
        :return: the decorated function
        """

        @wraps(func)
        def wrapped_func(request, chembl_id, *args, **kwargs):
            """
            :param request: request received
            :param args: args passed to the function
            :param kwargs: keyword args passed to the function
            :return: if the entity is different from the desired one
            """
            lookup_data = get_lookup_data(desired_entity_type, chembl_id)

            id_exists = lookup_data.get('id_exists', False)
            downgraded = lookup_data.get('downgraded', False)
            if not id_exists or downgraded:
                return HttpResponseRedirect(reverse('id_lookup', args=[chembl_id.upper()]))

            redirect_to = lookup_data.get('redirect_to')
            if redirect_to is not None:
                return HttpResponseRedirect(reverse(redirect_to, args=[chembl_id.upper()]))

            return func(request, chembl_id, *args, **kwargs)

        return wrapped_func

    return wrap


def get_lookup_data(desired_entity_type, chembl_id):
    """
    :param desired_entity_type: the intended entity type
    :param chembl_id: id to lookup
    :return: a dict describing the data obtained form the lookup
    """
    lookup_url = f'{settings.ES_PROXY_API_BASE_URL_INTERNAL}/es_data/get_es_document/' \
                 f'chembl_chembl_id_lookup/{chembl_id}'

    lookup_requests = requests.get(lookup_url)
    status_code = lookup_requests.status_code
    if status_code == 404:
        return {
            'id_exists': False,
        }

    lookup_response = lookup_requests.json().get('_source')
    status = lookup_response.get('status')
    if status != 'ACTIVE':
        return {
            'id_exists': True,
            'downgraded': True,
        }

    entity_type = lookup_response.get('entity_type')
    if entity_type != desired_entity_type:
        redirect_to = ENTITY_NAME_TO_URL_NAME[entity_type]
        print('redirect_to: ', redirect_to)
        return {
            'id_exists': True,
            'redirect_to': redirect_to,
        }

    return {
        'id_exists': True,
    }
