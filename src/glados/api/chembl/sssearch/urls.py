from django.urls import include, re_path
from . import search_manager

urlpatterns = [
    re_path(r'blast-params/$', search_manager.get_blast_params),
]
