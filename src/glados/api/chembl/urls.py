from django.urls import include, re_path

urlpatterns = [
    re_path(r'sssearch/', include('glados.api.chembl.sssearch.urls')),
]
