from django.urls import include, re_path

urlpatterns = [
    re_path(r'chembl/', include('glados.api.chembl.urls')),
]
