class AssayReportCardApp extends glados.ReportCardApp

  # -------------------------------------------------------------
  # Initialisation
  # -------------------------------------------------------------
  @init = ->
    super
    assay = AssayReportCardApp.getCurrentAssay()

    breadcrumbLinks = [
      {
        label: assay.get('id')
        link: Assay.get_report_card_url(assay.get('id'))
      }
    ]
    glados.apps.BreadcrumbApp.setBreadCrumb(breadcrumbLinks)

    AssayReportCardApp.initAssayBasicInformation()
    AssayReportCardApp.initCurationSummary()
    AssayReportCardApp.initVariantSequencesInfo()
    AssayReportCardApp.initAssayParams()
    AssayReportCardApp.initActivitySummary()
    AssayReportCardApp.initAssociatedCompounds()

    assay.fetch()

  # -------------------------------------------------------------
  # Specific section initialization
  # this is functions only initialize a section of the report card
  # -------------------------------------------------------------
  @initAssayBasicInformation = ->
    assay = AssayReportCardApp.getCurrentAssay()

    viewConfig =
      embed_section_name: 'assay_basic_information'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: assay
      el: $('#ABasicInformation')
      config: viewConfig
      section_id: 'BasicInformation'
      section_label: 'Basic Information'
      entity_name: Assay.prototype.entityName
      report_card_app: @
      vue_component: 'AssayBasicInformation'

  @initCurationSummary = ->
    assay = AssayReportCardApp.getCurrentAssay()

    viewConfig =
      embed_section_name: 'assay_curation_summary'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: assay
      el: $('#ACurationSummaryCard')
      config: viewConfig
      section_id: 'CurationSummary'
      section_label: 'Curation Summary'
      entity_name: Assay.prototype.entityName
      report_card_app: @
      vue_component: 'AssayCurationSummary'

  @initAssayParams = ->
    assay = AssayReportCardApp.getCurrentAssay()

    viewConfig =
      embed_section_name: 'assay_parameters'
      embed_identifier: assay.get('assay_chembl_id')
      show_if: (model) ->
        if not model.attributes.assay_parameters?
          return false
        if model.attributes.assay_parameters.length == 0
          return false
        return true

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: assay
      el: $('#AssayParametersDataCard')
      config: viewConfig
      section_id: 'AssayParameters'
      section_label: 'Assay Parameters'
      entity_name: Assay.prototype.entityName
      report_card_app: @
      vue_component: 'AssayParameters'

  @initActivitySummary = ->
    assay = AssayReportCardApp.getCurrentAssay()

    viewConfig =
      embed_section_name: 'bioactivities'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: assay
      el: $('#AActivityChartsCard')
      config: viewConfig
      section_id: 'ActivityCharts'
      section_label: 'Activity Charts'
      entity_name: Assay.prototype.entityName
      report_card_app: @
      vue_component: 'AssayActivityCharts'


  @initAssociatedCompounds = ->
    assay = AssayReportCardApp.getCurrentAssay()

    viewConfig =
      embed_section_name: 'associated_compounds'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: assay
      el: $('#AAssociatedCompoundProperties')
      config: viewConfig
      section_id: 'CompoundSummaries'
      section_label: 'Compound Summary'
      entity_name: Assay.prototype.entityName
      report_card_app: @
      vue_component: 'AssayAssociatedCompounds'

  @initVariantSequencesInfo = ->
    assay = AssayReportCardApp.getCurrentAssay()

    viewConfig =
      embed_section_name: 'variant_sequences_info'
      embed_identifier: assay.get('id')
      show_if: (model) -> model.attributes.variant_sequence?

    console.log('viewConfig: ', viewConfig)

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: assay
      el: $('#CVariantSequencesInfoCard')
      config: viewConfig
      section_id: 'VariantSequencesInfo'
      section_label: 'Variant Sequences Information'
      entity_name: Assay.prototype.entityName
      report_card_app: @
      vue_component: 'VariantSequencesInfo'

    if GlobalVariables['EMBEDED']
      assay.fetch()


  # -------------------------------------------------------------
  # Singleton
  # -------------------------------------------------------------
  @getCurrentAssay = ->
    if not @currentAssay?

      chemblID = glados.Utils.URLS.getCurrentModelChemblID()

      @currentAssay = new Assay
        assay_chembl_id: chemblID
      return @currentAssay

    else return @currentAssay

  # --------------------------------------------------------------------------------------------------------------------
  # Aggregations
  # --------------------------------------------------------------------------------------------------------------------
  @getAssociatedBioactivitiesAgg = (chemblID) ->
    queryConfig =
      type: glados.models.Aggregations.Aggregation.QueryTypes.QUERY_STRING
      query_string_template: 'assay_chembl_id:{{assay_chembl_id}}'
      template_data:
        assay_chembl_id: 'assay_chembl_id'

    aggsConfig =
      aggs:
        types:
          type: glados.models.Aggregations.Aggregation.AggTypes.TERMS
          field: 'standard_type'
          size: 20
          bucket_links:
            bucket_filter_template: 'assay_chembl_id:{{assay_chembl_id}} ' +
              'AND standard_type:("{{bucket_key}}"' +
              '{{#each extra_buckets}} OR "{{this}}"{{/each}})'
            template_data:
              assay_chembl_id: 'assay_chembl_id'
              bucket_key: 'BUCKET.key'
              extra_buckets: 'EXTRA_BUCKETS.key'

            link_generator: Activity.getActivitiesListURL

    bioactivities = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.ACTIVITY_INDEX_URL
      query_config: queryConfig
      assay_chembl_id: chemblID
      aggs_config: aggsConfig

    return bioactivities

  @getAssociatedCompoundsAgg = (chemblID, minCols = 1, maxCols = 20, defaultCols = 10) ->
    queryConfig =
      type: glados.models.Aggregations.Aggregation.QueryTypes.MULTIMATCH
      queryValueField: 'assay_chembl_id'
      fields: ['_metadata.related_assays.all_chembl_ids']

    aggsConfig =
      aggs:
        x_axis_agg:
          field: 'molecule_properties.full_mwt'
          type: glados.models.Aggregations.Aggregation.AggTypes.RANGE
          min_columns: minCols
          max_columns: maxCols
          num_columns: defaultCols
          bucket_links:
            bucket_filter_template: '_metadata.related_assays.all_chembl_ids:{{assay_chembl_id}} ' +
              'AND molecule_properties.full_mwt:(>={{min_val}} AND <={{max_val}})'
            template_data:
              assay_chembl_id: 'assay_chembl_id'
              min_val: 'BUCKET.from'
              max_val: 'BUCKETS.to'
            link_generator: Compound.getCompoundsListURL

    associatedCompounds = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.COMPOUND_INDEX_URL
      query_config: queryConfig
      assay_chembl_id: chemblID
      aggs_config: aggsConfig

    return associatedCompounds

  @getRelatedTargetsAgg = (chemblID) ->
    queryConfig =
      type: glados.models.Aggregations.Aggregation.QueryTypes.MULTIMATCH
      queryValueField: 'assay_chembl_id'
      fields: ['_metadata.related_assays.all_chembl_ids']

    aggsConfig =
      aggs:
        types:
          type: glados.models.Aggregations.Aggregation.AggTypes.TERMS
          field: 'target_type'
          size: 20
          bucket_links:
            bucket_filter_template: '_metadata.related_assays.all_chembl_ids:{{assay_chembl_id}} ' +
              'AND target_type:("{{bucket_key}}"' +
              '{{#each extra_buckets}} OR "{{this}}"{{/each}})'
            template_data:
              assay_chembl_id: 'assay_chembl_id'
              bucket_key: 'BUCKET.key'
              extra_buckets: 'EXTRA_BUCKETS.key'

            link_generator: Target.getTargetsListURL

    targetTypes = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.TARGET_INDEX_URL
      query_config: queryConfig
      assay_chembl_id: chemblID
      aggs_config: aggsConfig

    return targetTypes

  @getRelatedTargetsByClassAgg = (chemblID) ->
    queryConfig =
      type: glados.models.Aggregations.Aggregation.QueryTypes.MULTIMATCH
      queryValueField: 'assay_chembl_id'
      fields: ['_metadata.related_assays.all_chembl_ids']

    aggsConfig =
      aggs:
        classes:
          type: glados.models.Aggregations.Aggregation.AggTypes.TERMS
          field: '_metadata.protein_classification.l1'
          size: 20
          bucket_links:
            bucket_filter_template: '_metadata.related_assays.all_chembl_ids:{{assay_chembl_id}} ' +
              'AND _metadata.protein_classification.l1:("{{bucket_key}}"' +
              '{{#each extra_buckets}} OR "{{this}}"{{/each}})'
            template_data:
              assay_chembl_id: 'assay_chembl_id'
              bucket_key: 'BUCKET.key'
              extra_buckets: 'EXTRA_BUCKETS.key'

            link_generator: Target.getTargetsListURL

    targetTypes = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.TARGET_INDEX_URL
      query_config: queryConfig
      assay_chembl_id: chemblID
      aggs_config: aggsConfig

    return targetTypes

  # --------------------------------------------------------------------------------------------------------------------
  # mini Histograms
  # --------------------------------------------------------------------------------------------------------------------
  @initMiniCompoundsHistogram = ($containerElem, chemblID) ->
    associatedCompounds = AssayReportCardApp.getAssociatedCompoundsAgg(chemblID, minCols = 8,
      maxCols = 8, defaultCols = 8)

    config =
      max_categories: 8
      fixed_bar_width: true
      hide_title: false
      x_axis_prop_name: 'x_axis_agg'
      properties:
        mwt: glados.models.visualisation.PropertiesFactory.getPropertyConfigFor('Compound', 'FULL_MWT')
      initial_property_x: 'mwt'

    new glados.views.Visualisation.HistogramView
      model: associatedCompounds
      el: $containerElem
      config: config

    associatedCompounds.fetch()

  @initMiniActivitiesHistogram = ($containerElem, chemblID) ->
    bioactivities = AssayReportCardApp.getAssociatedBioactivitiesAgg(chemblID)

    stdTypeProp = glados.models.visualisation.PropertiesFactory.getPropertyConfigFor('Activity', 'STANDARD_TYPE',
      withColourScale = true)

    barsColourScale = stdTypeProp.colourScale

    config =
      max_categories: 8
      bars_colour_scale: barsColourScale
      fixed_bar_width: true
      hide_title: false
      x_axis_prop_name: 'types'
      properties:
        std_type: stdTypeProp
      initial_property_x: 'std_type'

    new glados.views.Visualisation.HistogramView
      model: bioactivities
      el: $containerElem
      config: config

    bioactivities.fetch()

  @initMiniHistogramFromFunctionLink = ->
    $clickedLink = $(@)

    [paramsList, constantParamsList, $containerElem] = \
      glados.views.PaginatedViews.PaginatedTable.prepareAndGetParamsFromFunctionLinkCell($clickedLink)

    histogramType = constantParamsList[0]
    chemblID = paramsList[0]

    if histogramType == 'compounds'
      AssayReportCardApp.initMiniCompoundsHistogram($containerElem, chemblID)
    else if histogramType == 'activities'
      AssayReportCardApp.initMiniActivitiesHistogram($containerElem, chemblID)