class DocumentReportCardApp extends glados.ReportCardApp

  # -------------------------------------------------------------
  # Initialization
  # -------------------------------------------------------------
  @init = ->
    super
    document = DocumentReportCardApp.getCurrentDocument()

    breadcrumbLinks = [
      {
        label: document.get('id')
        link: Document.get_report_card_url(document.get('id'))
      }
    ]
    glados.apps.BreadcrumbApp.setBreadCrumb(breadcrumbLinks)

    DocumentReportCardApp.initBasicInformation()
    DocumentReportCardApp.initRelatedDocuments()
    DocumentReportCardApp.initActivitySummary()
    DocumentReportCardApp.initCompoundSummary()

    document.fetch()

  # -------------------------------------------------------------
  # Singleton
  # -------------------------------------------------------------
  @getCurrentDocument = ->
    if not @currentDocument?

      chemblID = glados.Utils.URLS.getCurrentModelChemblID()

      @currentDocument = new Document
        document_chembl_id: chemblID
        fetch_from_elastic: true
      return @currentDocument

    else return @currentDocument

  # -------------------------------------------------------------
  # Specific section initialization
  # this is functions only initialize a section of the report card
  # -------------------------------------------------------------
  @initBasicInformation = ->
    document = DocumentReportCardApp.getCurrentDocument()

    viewConfig =
      embed_section_name: 'document_basic_information'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: document
      el: $('#DBasicInformation')
      config: viewConfig
      section_id: 'BasicInformation'
      section_label: 'Basic Information'
      entity_name: Document.prototype.entityName
      report_card_app: @
      vue_component: 'DocumentBasicInformation'

  @initRelatedDocuments = ->
    document = DocumentReportCardApp.getCurrentDocument()

    viewConfig =
      embed_section_name: 'document_related_documents'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    theView = new glados.views.ReportCards.EmbeddedVueInCardView
      model: document
      el: $('#CRelatedDocumentsCard')
      config: viewConfig
      section_id: 'RelatedDocuments'
      section_label: 'Related Documents'
      entity_name: Document.prototype.entityName
      report_card_app: @
      vue_component: 'RelatedDocuments'

    relatedDocsURL = "#{glados.Settings.ES_PROXY_API_BASE_URL}/es_data/get_es_document/chembl_document" +
      "/#{glados.Utils.URLS.getCurrentModelChemblID()}?source=_metadata.similar_documents"

    $.get(relatedDocsURL).done((response) ->
      similarDocs = glados.Utils.getNestedValue(response._source, '_metadata.similar_documents',
        forceAsNumber = false, customNullValueLabel = undefined, returnUndefined = true)

      if not similarDocs?
        theView.hideSection()
        theView.forceHidden = true
    )

  @initActivitySummary = ->
    document = DocumentReportCardApp.getCurrentDocument()

    viewConfig =
      embed_section_name: 'related_activities'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: document
      el: $('#DActivityChartsCard')
      config: viewConfig
      section_id: 'ActivityCharts'
      section_label: 'Activity Charts'
      entity_name: Document.prototype.entityName
      report_card_app: @
      vue_component: 'DocumentActivityCharts'

  @initCompoundSummary = ->
    document = DocumentReportCardApp.getCurrentDocument()

    viewConfig =
      embed_section_name: 'related_activities'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: document
      el: $('#DAssociatedCompoundPropertiesCard')
      config: viewConfig
      section_id: 'CompoundSummaries'
      section_label: 'Compound Summaries'
      entity_name: Document.prototype.entityName
      report_card_app: @
      vue_component: 'DocumentAssociatedCompounds'

  # -------------------------------------------------------------
  # Aggregations
  # -------------------------------------------------------------
  @getRelatedTargetsAggByClass = (chemblID) ->
    queryConfig =
      type: glados.models.Aggregations.Aggregation.QueryTypes.MULTIMATCH
      queryValueField: 'document_chembl_id'
      fields: ['_metadata.related_documents.all_chembl_ids']

    aggsConfig =
      aggs:
        classes:
          type: glados.models.Aggregations.Aggregation.AggTypes.TERMS
          field: '_metadata.protein_classification.l1'
          size: 20
          bucket_links:
            bucket_filter_template: '_metadata.related_documents.all_chembl_ids:{{document_chembl_id}} ' +
              'AND _metadata.protein_classification.l1:("{{bucket_key}}"' +
              '{{#each extra_buckets}} OR "{{this}}"{{/each}})'
            template_data:
              document_chembl_id: 'document_chembl_id'
              bucket_key: 'BUCKET.key'
              extra_buckets: 'EXTRA_BUCKETS.key'

            link_generator: Target.getTargetsListURL

    targetTypes = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.TARGET_INDEX_URL
      query_config: queryConfig
      document_chembl_id: chemblID
      aggs_config: aggsConfig

    return targetTypes

  @getRelatedTargetsAgg = (chemblID) ->
    queryConfig =
      type: glados.models.Aggregations.Aggregation.QueryTypes.MULTIMATCH
      queryValueField: 'document_chembl_id'
      fields: ['_metadata.related_documents.all_chembl_ids']

    aggsConfig =
      aggs:
        types:
          type: glados.models.Aggregations.Aggregation.AggTypes.TERMS
          field: 'target_type'
          size: 20
          bucket_links:
            bucket_filter_template: '_metadata.related_documents.all_chembl_ids:{{document_chembl_id}} ' +
              'AND target_type:("{{bucket_key}}"' +
              '{{#each extra_buckets}} OR "{{this}}"{{/each}})'
            template_data:
              document_chembl_id: 'document_chembl_id'
              bucket_key: 'BUCKET.key'
              extra_buckets: 'EXTRA_BUCKETS.key'

            link_generator: Target.getTargetsListURL

    targetTypes = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.TARGET_INDEX_URL
      query_config: queryConfig
      document_chembl_id: chemblID
      aggs_config: aggsConfig

    return targetTypes

  @getRelatedAssaysAgg = (chemblID) ->
    queryConfig =
      type: glados.models.Aggregations.Aggregation.QueryTypes.QUERY_STRING
      query_string_template: 'document_chembl_id:{{document_chembl_id}}'
      template_data:
        document_chembl_id: 'document_chembl_id'

    aggsConfig =
      aggs:
        types:
          type: glados.models.Aggregations.Aggregation.AggTypes.TERMS
          field: '_metadata.assay_generated.type_label'
          size: 20
          bucket_links:
            bucket_filter_template: 'document_chembl_id:{{document_chembl_id}} ' +
              'AND _metadata.assay_generated.type_label:("{{bucket_key}}"' +
              '{{#each extra_buckets}} OR "{{this}}"{{/each}})'
            template_data:
              document_chembl_id: 'document_chembl_id'
              bucket_key: 'BUCKET.key'
              extra_buckets: 'EXTRA_BUCKETS.key'

            link_generator: Assay.getAssaysListURL

    assays = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.ASSAY_INDEX_URL
      query_config: queryConfig
      document_chembl_id: chemblID
      aggs_config: aggsConfig

    return assays

  @getRelatedActivitiesAgg = (chemblID) ->
    queryConfig =
      type: glados.models.Aggregations.Aggregation.QueryTypes.QUERY_STRING
      query_string_template: 'document_chembl_id:{{document_chembl_id}}'
      template_data:
        document_chembl_id: 'document_chembl_id'

    aggsConfig =
      aggs:
        types:
          type: glados.models.Aggregations.Aggregation.AggTypes.TERMS
          field: 'standard_type'
          size: 20
          bucket_links:
            bucket_filter_template: 'document_chembl_id:{{document_chembl_id}} ' +
              'AND standard_type:("{{bucket_key}}"' +
              '{{#each extra_buckets}} OR "{{this}}"{{/each}})'
            template_data:
              document_chembl_id: 'document_chembl_id'
              bucket_key: 'BUCKET.key'
              extra_buckets: 'EXTRA_BUCKETS.key'

            link_generator: Activity.getActivitiesListURL

    bioactivities = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.ACTIVITY_INDEX_URL
      query_config: queryConfig
      document_chembl_id: chemblID
      aggs_config: aggsConfig

    return bioactivities

  @getAssociatedCompoundsAgg = (chemblID, minCols = 1, maxCols = 20, defaultCols = 10) ->
    queryConfig =
      type: glados.models.Aggregations.Aggregation.QueryTypes.MULTIMATCH
      queryValueField: 'document_chembl_id'
      fields: ['_metadata.related_documents.all_chembl_ids']

    aggsConfig =
      aggs:
        x_axis_agg:
          field: 'molecule_properties.full_mwt'
          type: glados.models.Aggregations.Aggregation.AggTypes.RANGE
          min_columns: minCols
          max_columns: maxCols
          num_columns: defaultCols
          bucket_links:
            bucket_filter_template: '_metadata.related_documents.all_chembl_ids:{{document_chembl_id}} ' +
              'AND molecule_properties.full_mwt:(>={{min_val}} AND <={{max_val}})'
            template_data:
              document_chembl_id: 'document_chembl_id'
              min_val: 'BUCKET.from'
              max_val: 'BUCKETS.to'
            link_generator: Compound.getCompoundsListURL

    associatedCompounds = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.COMPOUND_INDEX_URL
      query_config: queryConfig
      document_chembl_id: chemblID
      aggs_config: aggsConfig

    return associatedCompounds

  # --------------------------------------------------------------------------------------------------------------------
  # mini Histograms
  # --------------------------------------------------------------------------------------------------------------------
  @initMiniActivitiesHistogram = ($containerElem, chemblID) ->
    bioactivities = DocumentReportCardApp.getRelatedActivitiesAgg(chemblID)

    stdTypeProp = glados.models.visualisation.PropertiesFactory.getPropertyConfigFor('Activity', 'STANDARD_TYPE',
      withColourScale = true)

    barsColourScale = stdTypeProp.colourScale

    config =
      max_categories: 8
      bars_colour_scale: barsColourScale
      fixed_bar_width: true
      hide_title: false
      x_axis_prop_name: 'types'
      properties:
        std_type: stdTypeProp
      initial_property_x: 'std_type'

    new glados.views.Visualisation.HistogramView
      model: bioactivities
      el: $containerElem
      config: config

    bioactivities.fetch()

  @initMiniCompoundsHistogram = ($containerElem, chemblID) ->
    associatedCompounds = DocumentReportCardApp.getAssociatedCompoundsAgg(chemblID, minCols = 8,
      maxCols = 8, defaultCols = 8)

    config =
      max_categories: 8
      fixed_bar_width: true
      hide_title: false
      x_axis_prop_name: 'x_axis_agg'
      properties:
        mwt: glados.models.visualisation.PropertiesFactory.getPropertyConfigFor('Compound', 'FULL_MWT')
      initial_property_x: 'mwt'

    new glados.views.Visualisation.HistogramView
      model: associatedCompounds
      el: $containerElem
      config: config

    associatedCompounds.fetch()

  @initMiniTargetsHistogram = ($containerElem, chemblID) ->
    targetTypes = DocumentReportCardApp.getRelatedTargetsAgg(chemblID)

    stdTypeProp = glados.models.visualisation.PropertiesFactory.getPropertyConfigFor('Target', 'TARGET_TYPE',
      withColourScale = true)

    barsColourScale = stdTypeProp.colourScaleRnage

    config =
      max_categories: 8
      bars_colour_scale: barsColourScale
      fixed_bar_width: true
      hide_title: false
      x_axis_prop_name: 'types'
      properties:
        std_type: stdTypeProp
      initial_property_x: 'std_type'

    new glados.views.Visualisation.HistogramView
      model: targetTypes
      el: $containerElem
      config: config

    targetTypes.fetch()

  @initMiniHistogramFromFunctionLink = ->
    $clickedLink = $(@)

    [paramsList, constantParamsList, $containerElem] = \
      glados.views.PaginatedViews.PaginatedTable.prepareAndGetParamsFromFunctionLinkCell($clickedLink)

    histogramType = constantParamsList[0]
    chemblID = paramsList[0]

    if histogramType == 'activities'
      DocumentReportCardApp.initMiniActivitiesHistogram($containerElem, chemblID)
    else if histogramType == 'compounds'
      DocumentReportCardApp.initMiniCompoundsHistogram($containerElem, chemblID)
    else if histogramType == 'targets'
      DocumentReportCardApp.initMiniTargetsHistogram($containerElem, chemblID)



