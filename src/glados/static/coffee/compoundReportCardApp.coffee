# This takes care of handling the compound report card.
class CompoundReportCardApp extends glados.ReportCardApp

  #This initializes all views and models necessary for the compound report card
  @init = ->
    super

    compound = CompoundReportCardApp.getCurrentCompound()

    breadcrumbLinks = [
      {
        label: compound.get('id')
        link: Compound.get_report_card_url(compound.get('id'))
      }
    ]
    glados.apps.BreadcrumbApp.setBreadCrumb(breadcrumbLinks)

    CompoundReportCardApp.initNameAndClassification()
    CompoundReportCardApp.initRepresentations()
    CompoundReportCardApp.initSources()
    CompoundReportCardApp.initAlternateForms()
    CompoundReportCardApp.initMoleculeFeatures()
    CompoundReportCardApp.initProdrugInformation()
    CompoundReportCardApp.initIndications()
    CompoundReportCardApp.initMechanismOfAction()
    CompoundReportCardApp.initWithdrawnInfo()
    CompoundReportCardApp.initSimilarCompounds()
    CompoundReportCardApp.initMetabolism()
    CompoundReportCardApp.initBioSeq()
    CompoundReportCardApp.initHELMNotation()
    CompoundReportCardApp.initActivitySummary()
    CompoundReportCardApp.initPapersAboutCompound()
    CompoundReportCardApp.initTargetPredictions()
    CompoundReportCardApp.initCalculatedCompoundParentProperties()
    CompoundReportCardApp.initStructuralAlerts()
    CompoundReportCardApp.initCrossReferences()
    CompoundReportCardApp.initUniChemCrossReferences()
    CompoundReportCardApp.initUnichemConnectivityMatches()

    compound.fetch()

    ButtonsHelper.initCroppedContainers()
    ButtonsHelper.initExpendableMenus()

  # -------------------------------------------------------------
  # Singleton
  # -------------------------------------------------------------
  @getCurrentCompound = ->
    if not @currentCompound?

      chemblID = glados.Utils.URLS.getCurrentModelChemblID()
      @currentCompound = new Compound
        molecule_chembl_id: chemblID
      return @currentCompound

    else return @currentCompound

  # -------------------------------------------------------------
  # Specific section initialization
  # this is functions only initialize a section of the report card
  # -------------------------------------------------------------
  @initNameAndClassification = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'name_and_classification'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#CNCCard')
      config: viewConfig
      section_id: 'CompoundNameAndClassification'
      section_label: 'Name And Classification'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'NameAndClassification'

    if GlobalVariables['EMBEDED']
      compound.fetch()

      ButtonsHelper.initExpendableMenus()

  @initRepresentations = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'representations'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()
      show_if: (model) ->
        return model.attributes.molecule_structures?

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#CompRepsCard')
      config: viewConfig
      section_id: 'CompoundRepresentations'
      section_label: 'Representations'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'Representations'

    if GlobalVariables['EMBEDED']
      compound.fetch()

  @initSources = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'sources'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()
      show_if: (model) ->
        compoundRecords = glados.Utils.getNestedValue(model.attributes, '_metadata.compound_records',
          forceAsNumber = false, customNullValueLabel = undefined, returnUndefined = true)

        if not compoundRecords?
          return false
        else if compoundRecords.length == 0
          return false
        else
          return true

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#CSourcesCard')
      config: viewConfig
      section_id: 'CompoundSources'
      section_label: 'Sources'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'Sources'

    if GlobalVariables['EMBEDED']
      compound.fetch()

  @initCalculatedCompoundParentProperties = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'sources'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()
      show_if: (model) ->
        moleculeProperties = glados.Utils.getNestedValue(model.attributes, 'molecule_properties',
          forceAsNumber = false, customNullValueLabel = undefined, returnUndefined = true)

        return moleculeProperties?

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#CalculatedParentPropertiesCard')
      config: viewConfig
      section_id: 'CalculatedCompoundParentProperties'
      section_label: 'Calculated Properties'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'CalculatedProperties'

    if GlobalVariables['EMBEDED']
      compound.fetch()

  @initMechanismOfAction = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'mechanism_of_action'
      embed_identifier: compound.get('molecule_chembl_id')
      show_if: (model) ->
        if not model.attributes._metadata.generated_resources?
          return false
        return model.attributes._metadata.generated_resources.has_mechanisms == true

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#MechOfActCard')
      config: viewConfig
      section_id: 'MechanismOfAction'
      section_label: 'Drug Mechanisms'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'DrugMechanisms'

    if GlobalVariables['EMBEDED']
      compound.fetch()

  @initIndications = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'drug_indications'
      embed_identifier: compound.get('molecule_chembl_id')
      show_if: (model) ->
        if not model.attributes._metadata.generated_resources?
          return false
        return model.attributes._metadata.generated_resources.has_drug_indications == true

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#CDrugIndicationsCard')
      config: viewConfig
      section_id: 'Indications'
      section_label: 'Drug Indications'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'DrugIndications'

    if GlobalVariables['EMBEDED']
      compound.fetch()

  @initMoleculeFeatures = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'molecule_features'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#MoleculeFeaturesCard')
      config: viewConfig
      section_id: 'MoleculeFeatures'
      section_label: 'Molecule Features'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'MoleculeFeatures'

    if GlobalVariables['EMBEDED']
      compound.fetch()

  @initProdrugInformation = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'prodrug'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()
      show_if: (model) ->
        prodrug_flag = glados.Utils.getNestedValue(model.attributes, 'prodrug',
          forceAsNumber = false, customNullValueLabel = undefined, returnUndefined = true)

        return prodrug_flag == 1

    theView = new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#ProdrugInformationCard')
      config: viewConfig
      section_id: 'ProdrugInformation'
      section_label: 'Prodrug Information'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'ProdrugInformation'


    if GlobalVariables['EMBEDED']
      compound.fetch()

  @initWithdrawnInfo = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'withdrawal_info'
      embed_identifier: compound.get('molecule_chembl_id')
      show_if: (model) ->
        if not model.attributes._metadata.generated_resources?
          return false
        return model.attributes._metadata.generated_resources.has_drug_warnings == true
      properties_group: 'withdrawal_info'

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#CWithdrawnInfoCard')
      config: viewConfig
      section_id: 'WithdrawnInfo'
      section_label: 'Drug Warning Information'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'WithdrawnInfo'

    if GlobalVariables['EMBEDED']
      compound.fetch()

  @initStructuralAlerts = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'molecule_features'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()
      show_if: (model) ->
        compound_structural_alerts = glados.Utils.getNestedValue(model.attributes,
          '_metadata.compound_structural_alerts',
          forceAsNumber = false, customNullValueLabel = undefined, returnUndefined = true)

        if not compound_structural_alerts?
          return false

        if compound_structural_alerts.length == 0
          return false

        return true

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#CStructuralAlertsCard')
      config: viewConfig
      section_id: 'StructuralAlerts'
      section_label: 'Structural Alerts'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'StructuralAlerts'

  @initAlternateForms = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'alternate_forms'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#AlternateFormsCard')
      config: viewConfig
      section_id: 'AlternateFormsOfCompoundInChEMBL'
      section_label: 'Alternative Forms'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'AlternativeForms'

    if GlobalVariables['EMBEDED']
      compound.fetch()

  @initActivitySummary = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'activity_charts'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#CActivityChartsCard')
      config: viewConfig
      section_id: 'ActivityCharts'
      section_label: 'Activity Charts'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'CompoundActivityCharts'

  @initPapersAboutCompound = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'papers_per_year'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#PapersAboutCompoundPerYear')
      config: viewConfig
      section_id: 'PapersAboutCompound'
      section_label: 'Literature'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'CompoundLiterature'

  @initTargetPredictions = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'target_predictions'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()
      show_if: (model, context) ->
        molecule_structures = model.attributes.molecule_structures

        if not molecule_structures?
          return false

        canonical_smiles = glados.Utils.getNestedValue(model.attributes,
          'molecule_structures.canonical_smiles',
          forceAsNumber = false, customNullValueLabel = undefined, returnUndefined = true)

        if not canonical_smiles?
          return false

        # checks if there are predictions for this compound
        url = "https://www.ebi.ac.uk/chembl/target-predictions"

        deferred = $.post
          url: url
          data: JSON.stringify({'smiles': canonical_smiles})
          dataType: 'json'
          contentType: 'application/json'
          mimeType: 'application/json'

        deferred.done (data) ->
          if data.length > 0
            context.showSection()
            context.showCardContent()
          else
            context.hideSection()

        deferred.fail (jqxhrError) ->
          context.hideSection()

        return true

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#CTargetPredictionsCard')
      config: viewConfig
      section_id: 'TargetPredictions'
      section_label: 'Target Predictions'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'TargetPredictions'

    if GlobalVariables['EMBEDED']
      compound.fetch()

  @initCrossReferences = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'cross_refs'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()
      show_if: (model) ->
        references = model.get('cross_references')
        if not references?
          return false

        if references.length == 0
          return false

        return true

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#CrossReferencesCard')
      config: viewConfig
      section_id: 'CompoundCrossReferences'
      section_label: 'Cross References'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'CompoundXRefs'

    if GlobalVariables['EMBEDED']
      compound.fetch()

  @initUniChemCrossReferences = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'unichem_cross_refs'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()
      show_if: (model) ->
        references = model.get('_metadata').unichem
        if not references?
          return false

        if references.length == 0
          return false

        return true


    new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#UniChemCrossReferencesCard')
      config: viewConfig
      section_id: 'UniChemCrossReferences'
      section_label: 'UniChem Cross References'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'CompoundUnichemXRefs'

    if GlobalVariables['EMBEDED']
      compound.fetch()

  @initUnichemConnectivityMatches = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'unichem_connectivity_matches'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#CUnichemConnectivityMatchesCard')
      config: viewConfig
      section_id: 'UniChemConnectivityMatches'
      section_label: 'UniChem Connectivity Layer Cross References'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'UnichemConnectivityMatches'

    if GlobalVariables['EMBEDED']
      compound.fetch()

  @initSimilarCompounds = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'similar'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()
      show_if: (model) ->
        structures = glados.Utils.getNestedValue(model.attributes, 'molecule_structures',
          forceAsNumber = false, customNullValueLabel = undefined, returnUndefined = true)

        if not structures?
          return false

        return true

    theView = new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#SimilarCompoundsCard')
      config: viewConfig
      section_id: 'SimilarCompounds'
      section_label: 'Similar Compounds'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'SimilarCompounds'

    chemblID = compound.get('molecule_chembl_id')
    similarityURL = glados.Settings.WS_BASE_URL + 'similarity/' + chemblID + '/85.json'

    $.get(similarityURL).done((response) ->
      totalCount = response.page_meta.total_count
      if totalCount == 0
        theView.hideSection()
        theView.forceHidden = true
    )

    if GlobalVariables['EMBEDED']
      compound.fetch()

  @initMetabolismFullScreen = ->
    GlobalVariables.CHEMBL_ID = URLProcessor.getUrlPartInReversePosition 0
    compoundMetabolism = new glados.models.Compound.Metabolism
      molecule_chembl_id: GlobalVariables.CHEMBL_ID

    new CompoundMetabolismFSView
      model: compoundMetabolism
      el: $('#CompoundMetabolismMain')

    compoundMetabolism.fetch()

  @initMetabolism = ->
    compoundMetabolism = new glados.models.Compound.Metabolism
      molecule_chembl_id: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.MetabolismInCardView
      model: compoundMetabolism
      el: $('#MetabolismCard')
      molecule_chembl_id: glados.Utils.URLS.getCurrentModelChemblID()
      section_id: 'Metabolism'
      section_label: 'Metabolism'
      entity_name: Compound.prototype.entityName
      report_card_app: @

    compoundMetabolism.fetch()

  #https://chembl-glados.herokuapp.com/compound_report_card/CHEMBL1201585/
  @initHELMNotation = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'helm_notation'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()
      show_if: (model) ->
        helm_notation = glados.Utils.getNestedValue(model.attributes,
          'helm_notation',
          forceAsNumber = false, customNullValueLabel = undefined, returnUndefined = true)

        return helm_notation?


    new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#CHELMNotationCard')
      config: viewConfig
      section_id: 'CompoundHELMNotation'
      section_label: 'HELM Notation'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'HelmNotation'

    if GlobalVariables['EMBEDED']
      compound.fetch()

  # https://chembl-glados.herokuapp.com/compound_report_card/CHEMBL1201585
  @initBioSeq = ->
    compound = CompoundReportCardApp.getCurrentCompound()

    viewConfig =
      embed_section_name: 'biocomponents'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()
      show_if: (model) ->
        biocomponents = glados.Utils.getNestedValue(model.attributes,
          'biotherapeutic.biocomponents',
          forceAsNumber = false, customNullValueLabel = undefined, returnUndefined = true)

        if not biocomponents?
          return false

        if biocomponents.length == 0
          return false

        return true

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: compound
      el: $('#CBioseqCard')
      config: viewConfig
      section_id: 'CompoundBIOLSeq'
      section_label: 'Biocomponents'
      entity_name: Compound.prototype.entityName
      report_card_app: @
      vue_component: 'Biocomponents'

    if GlobalVariables['EMBEDED']
      compound.fetch()

  # -------------------------------------------------------------
  # Function Cells
  # -------------------------------------------------------------
  @initMiniBioactivitiesHistogram = ($containerElem, chemblID) ->
    bioactivities = CompoundReportCardApp.getRelatedActivitiesAgg([chemblID])

    stdTypeProp = glados.models.visualisation.PropertiesFactory.getPropertyConfigFor('Activity', 'STANDARD_TYPE',
      withColourScale = true)

    barsColourScale = stdTypeProp.colourScale

    config =
      max_categories: 8
      bars_colour_scale: barsColourScale
      fixed_bar_width: true
      hide_title: false
      x_axis_prop_name: 'types'
      properties:
        std_type: stdTypeProp
      initial_property_x: 'std_type'

    new glados.views.Visualisation.HistogramView
      model: bioactivities
      el: $containerElem
      config: config

    bioactivities.fetch()

  @initMiniTargetsByClassHistogram = ($containerElem, chemblID) ->
    targetClases = CompoundReportCardApp.getRelatedTargetsAggByClass(chemblID)

    stdClassProp = glados.models.visualisation.PropertiesFactory.getPropertyConfigFor('Target', 'TARGET_CLASS',
      withColourScale = true)

    barsColourScale = stdClassProp.colourScale

    config =
      max_categories: 8
      bars_colour_scale: barsColourScale
      fixed_bar_width: true
      hide_title: false
      x_axis_prop_name: 'classes'
      properties:
        target_class: stdTypeProp
      initial_property_x: 'target_class'

    new glados.views.Visualisation.HistogramView
      model: targetTypes
      el: $containerElem
      config: config

    targetTypes.fetch()


  @initMiniTargetsHistogram = ($containerElem, chemblID) ->
    targetTypes = CompoundReportCardApp.getRelatedTargetsAgg(chemblID)

    stdTypeProp = glados.models.visualisation.PropertiesFactory.getPropertyConfigFor('Target', 'TARGET_TYPE',
      withColourScale = true)

    barsColourScale = stdTypeProp.colourScale

    config =
      max_categories: 8
      bars_colour_scale: barsColourScale
      fixed_bar_width: true
      hide_title: false
      x_axis_prop_name: 'types'
      properties:
        std_type: stdTypeProp
      initial_property_x: 'std_type'

    new glados.views.Visualisation.HistogramView
      model: targetTypes
      el: $containerElem
      config: config

    targetTypes.fetch()

  @initMiniHistogramFromFunctionLink = ->
    $clickedLink = $(@)

    [paramsList, constantParamsList, $containerElem] = \
      glados.views.PaginatedViews.PaginatedTable.prepareAndGetParamsFromFunctionLinkCell($clickedLink)

    histogramType = constantParamsList[0]
    compoundChemblID = paramsList[0]

    if histogramType == 'activities'
      CompoundReportCardApp.initMiniBioactivitiesHistogram($containerElem, compoundChemblID)
    else if histogramType == 'targets'
      CompoundReportCardApp.initMiniTargetsHistogram($containerElem, compoundChemblID)
    else if histogramType == 'targets_by_class'
      CompoundReportCardApp.initMiniTargetsByClassHistogram($containerElem, compoundChemblID)

  # --------------------------------------------------------------------------------------------------------------------
  # Cells Functions
  # --------------------------------------------------------------------------------------------------------------------
  @initStructuralAlertsCarouselFromFunctionLink = ->
    $clickedLink = $(@)
    [paramsList, constantParamsList, $containerElem, objectParam] = \
      glados.views.PaginatedViews.PaginatedTable.prepareAndGetParamsFromFunctionLinkCell($clickedLink, isDataVis = false)


    structAlerts = glados.models.paginatedCollections.PaginatedCollectionFactory.getNewStructuralAlertList()

    $carouselContainer = $("<div>#{glados.Utils.getContentFromTemplate('Handlebars-Common-DefaultCarouselContent')}</div>")
    $containerElem.append($carouselContainer)
    glados.views.PaginatedViews.PaginatedViewFactory.getNewCardsCarouselView(structAlerts, $containerElem)

    parsedAlerts = JSON.parse(objectParam)
    structAlerts.setMeta('data_loaded', true)
    structAlerts.reset(_.map(parsedAlerts, glados.models.Compound.StructuralAlert.prototype.parse))

  @initDrugIconGridFromFunctionLink = ->
    $clickedLink = $(@)
    [paramsList, constantParamsList, $containerElem] = \
      glados.views.PaginatedViews.PaginatedTable.prepareAndGetParamsFromFunctionLinkCell($clickedLink, isDataVis = false)

    $gridContainer = $('<div class="BCK-FeaturesGrid" data-hb-template="Handlebars-Compound-MoleculeFeaturesGrid">')
    $containerElem.append($gridContainer)

    chemblID = paramsList[0]
    # in the future this should be taken form the collection instead of creating a new instance
    compound = new Compound
      molecule_chembl_id: chemblID

    viewConfig =
      is_outside_an_entity_report_card: true

    new CompoundFeaturesView
      model: compound
      el: $containerElem
      table_cell_mode: true
      config: viewConfig
    compound.fetch()

  # --------------------------------------------------------------------------------------------------------------------
  # Aggregations
  # --------------------------------------------------------------------------------------------------------------------
  @getRelatedTargetsAgg = (chemblID) ->
    queryConfig =
      type: glados.models.Aggregations.Aggregation.QueryTypes.MULTIMATCH
      queryValueField: 'molecule_chembl_id'
      fields: ['_metadata.related_compounds.all_chembl_ids']

    aggsConfig =
      aggs:
        types:
          type: glados.models.Aggregations.Aggregation.AggTypes.TERMS
          field: 'target_type'
          size: 20
          bucket_links:
            bucket_filter_template: '_metadata.related_compounds.all_chembl_ids:{{molecule_chembl_id}} ' +
              'AND target_type:("{{bucket_key}}"' +
              '{{#each extra_buckets}} OR "{{this}}"{{/each}})'
            template_data:
              molecule_chembl_id: 'molecule_chembl_id'
              bucket_key: 'BUCKET.key'
              extra_buckets: 'EXTRA_BUCKETS.key'

            link_generator: Target.getTargetsListURL

    targetTypes = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.TARGET_INDEX_URL
      query_config: queryConfig
      molecule_chembl_id: chemblID
      aggs_config: aggsConfig

    return targetTypes

  @getRelatedTargetsAggByClass = (chemblIDs) ->
    queryConfig = glados.configs.ReportCards.Compound.TargetSummaryPieConfig.getQueryConfig()
    aggsConfig = glados.configs.ReportCards.Compound.TargetSummaryPieConfig.getAggConfig()

    targetTypes = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.TARGET_INDEX_URL
      query_config: queryConfig
      molecule_chembl_ids: chemblIDs
      aggs_config: aggsConfig

    return targetTypes

  @getRelatedAssaysAgg = (chemblIDs) ->
    queryConfig = glados.configs.ReportCards.Compound.AssaySummaryPieConfig.getQueryConfig()
    aggsConfig = glados.configs.ReportCards.Compound.AssaySummaryPieConfig.getAggConfig()

    assays = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.ASSAY_INDEX_URL
      query_config: queryConfig
      molecule_chembl_ids: chemblIDs
      aggs_config: aggsConfig

    return assays

  @getRelatedActivitiesAgg = (chemblIDs) ->
    queryConfig = glados.configs.ReportCards.Compound.ActivityPieSummaryConfig.getQueryConfig()
    aggsConfig = glados.configs.ReportCards.Compound.ActivityPieSummaryConfig.getAggConfig()

    bioactivities = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.ACTIVITY_INDEX_URL
      query_config: queryConfig
      molecule_chembl_ids: chemblIDs
      aggs_config: aggsConfig

    return bioactivities

  @getPapersPerYearAgg = (chemblID, defaultInterval = 1)  ->
    queryConfig =
      type: glados.models.Aggregations.Aggregation.QueryTypes.MULTIMATCH
      queryValueField: 'molecule_chembl_id'
      fields: ['_metadata.related_compounds.all_chembl_ids']

    aggsConfig =
      aggs:
        documentsPerYear:
          type: glados.models.Aggregations.Aggregation.AggTypes.HISTOGRAM
          field: 'year'
          default_interval_size: defaultInterval
          min_interval_size: 1
          max_interval_size: 10
          bucket_key_parse_function: (key) -> key.replace(/\.0/i, '')
          bucket_sort_compare_function: (bucketA, bucketB) ->
            yearA = parseFloat(bucketA.key)
            yearB = parseFloat(bucketB.key)
            if yearA < yearB
              return -1
            else if yearA > yearB
              return 1
            return 0
          aggs:
            journal:
              type: glados.models.Aggregations.Aggregation.AggTypes.TERMS
              field: 'journal'
              size: 10
              bucket_links:
                bucket_filter_template: '_metadata.related_compounds.all_chembl_ids:({{molecule_chembl_id}})' +
                  ' AND year:({{year}}) AND journal:("{{bucket_key}}"' +
                  '{{#each extra_buckets}} OR "{{this}}"{{/each}})'
                template_data:
                  year: 'BUCKET.parsed_parent_key'
                  bucket_key: 'BUCKET.key'
                  extra_buckets: 'EXTRA_BUCKETS.key'
                  molecule_chembl_id: 'molecule_chembl_id'

                link_generator: Document.getDocumentsListURL

    allDocumentsByYear = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.DOCUMENT_INDEX_URL
      query_config: queryConfig
      aggs_config: aggsConfig
      molecule_chembl_id: chemblID

    return allDocumentsByYear

