glados.useNameSpace 'glados.views.SearchResults',
  SSQueryView: Backbone.View.extend

    events:
      'click .BCK-Edit-Query': 'showEditModal'

    getStatusText: ->
      currentStatus = @model.getState()
      if currentStatus == glados.models.Search.StructureSearchModel.STATES.INITIAL_STATE
        return 'Submitting'
      else if currentStatus == glados.models.Search.StructureSearchModel.STATES.ERROR_STATE
        return 'There was an error. Please try again later.'
      else if currentStatus == glados.models.Search.StructureSearchModel.STATES.SEARCH_QUEUED
        return 'Submitted'
      else if currentStatus == glados.models.Search.StructureSearchModel.STATES.SEARCHING
        progress = @model.get('progress')
        if progress?
          return "Searching (#{progress}%)"
        else return 'Searching'
      else if currentStatus == glados.models.Search.StructureSearchModel.STATES.FINISHED
        return 'Results Ready'

    getStatusLink: ->
      currentStatus = @model.getState()
      searchType = @model.get('search_type')
      if currentStatus == glados.models.Search.StructureSearchModel.STATES.INITIAL_STATE
        return undefined

      if searchType == glados.models.Search.StructureSearchModel.SEARCH_TYPES.STRUCTURE.SIMILARITY
        queryParams = @model.get('query_params')
        smiles = queryParams.search_term
        threshold = queryParams.threshold
        directSimilarityURL = "#{glados.Settings.WS_BASE_URL}similarity/#{smiles}/#{threshold}.json"
        return directSimilarityURL

      if searchType == glados.models.Search.StructureSearchModel.SEARCH_TYPES.STRUCTURE.CONNECTIVITY

        queryParams = @model.get('query_params')
        searchTerm = queryParams.search_term
        directConnectivityURL = "#{glados.Settings.WS_BASE_URL}molecule.json?molecule_structures__canonical_smiles__flexmatch=#{searchTerm}"
        return directConnectivityURL

      return @model.getProgressURL()