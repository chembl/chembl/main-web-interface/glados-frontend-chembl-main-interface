glados.useNameSpace 'glados.views.ReportCards',
  EmbeddedVueInCardView: CardView.extend

    initialize: ->
      CardView.prototype.initialize.call(@, arguments)
      @config = arguments[0].config
      @model.on 'change', @.render, @
      @model.on 'error', @.showCompoundErrorCard, @
      @resource_type = arguments[0].entity_name
      @vue_component = arguments[0].vue_component
      @initEmbedModal(@config.embed_section_name, @config.embed_identifier)
      @activateModals()

    render: ->
      if @forceHidden
        return

      if @config.show_if?
        show = @config.show_if(@model, @)
        if not show
          @hideSection()
          return

      iframeURL = @getIframeURL()

      $iframeElem = $(@el).find('.BCK-Details-Container')
      renderParams = {
        embedded_vue_url: iframeURL
      }
      glados.Utils.fillContentForElement $iframeElem, renderParams
      glados.helpers.EmbeddedVueHelper.setIframeDynamicResizeListener($iframeElem, @vue_component)

      @showSection()
      @showCardContent()

    getIframeURL: ->
      if @vue_component == 'WithdrawnInfo'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/drug_warning_info/#{@model.get('id')}"

      if @vue_component == 'VariantSequencesInfo'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/assay/sections/variant_sequences/#{@model.get('id')}"

      if @vue_component == 'UnichemConnectivityMatches'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/unichem_connectivity/#{@model.get('id')}"

      if @vue_component == 'AlternativeForms'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/alternative_forms/#{@model.get('id')}"

      if @vue_component == 'SimilarCompounds'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/similar_compounds/#{@model.get('id')}"

      if @vue_component == 'ProdrugInformation'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/prodrug_info/#{@model.get('id')}"

      if @vue_component == 'DrugIndications'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/drug_indications/#{@model.get('id')}"

      if @vue_component == 'DrugMechanisms'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/drug_mechanisms/#{@model.get('id')}"

      if @vue_component == 'MoleculeFeatures'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/molecule_features/#{@model.get('id')}"

      if @vue_component == 'ApprovedDrugsAndClinicalCandidates'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/target/sections/approved_drugs_and_clinical_candidates/#{@model.get('id')}"

      if @vue_component == 'NameAndClassification'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/name_and_classification/#{@model.get('id')}"

      if @vue_component == 'Representations'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/representations/#{@model.get('id')}"

      if @vue_component == 'Sources'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/sources/#{@model.get('id')}"

      if @vue_component == 'CalculatedProperties'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/calculated_properties/#{@model.get('id')}"

      if @vue_component == 'StructuralAlerts'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/structural_alerts/#{@model.get('id')}"

      if @vue_component == 'HelmNotation'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/helm_notation/#{@model.get('id')}"

      if @vue_component == 'Biocomponents'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/biocomponents/#{@model.get('id')}"
      # TargetPredictions
      if @vue_component == 'TargetPredictions'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/target_predictions/#{@model.get('id')}"

      # CompoundXRefs
      if @vue_component == 'CompoundXRefs'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/x_refs/#{@model.get('id')}"

      # CompoundUnichemXRefs
      if @vue_component == 'CompoundUnichemXRefs'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/unichem_x_refs/#{@model.get('id')}"

      # CompoundActivityCharts
      if @vue_component == 'CompoundActivityCharts'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/activity_charts/#{@model.get('id')}"

      # CompoundLiterature
      if @vue_component == 'CompoundLiterature'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/compound/sections/literature/#{@model.get('id')}"

      # TargetGeneXRefs
      if @vue_component == 'TargetGeneXRefs'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/target/sections/gene_x_refs/#{@model.get('id')}"

      # TargetProteinXRefs
      if @vue_component == 'TargetProteinXRefs'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/target/sections/protein_x_refs/#{@model.get('id')}"

      # TargetDomainXRefs
      if @vue_component == 'TargetDomainXRefs'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/target/sections/domain_x_refs/#{@model.get('id')}"

      # TargetStructureXRefs
      if @vue_component == 'TargetStructureXRefs'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/target/sections/structure_x_refs/#{@model.get('id')}"

      # TargetComponents
      if @vue_component == 'TargetComponents'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/target/sections/components/#{@model.get('id')}"

      # TargetRelations
      if @vue_component == 'TargetRelations'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/target/sections/relations/#{@model.get('id')}"

      # TargetNameAndClassification
      if @vue_component == 'TargetNameAndClassification'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/target/sections/name_and_classification/#{@model.get('id')}"

      #TargetActivityCharts
      if @vue_component == 'TargetActivityCharts'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/target/sections/activity_charts/#{@model.get('id')}"

      # TargetAssociatedCompounds
      if @vue_component == 'TargetAssociatedCompounds'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/target/sections/associated_compounds/#{@model.get('id')}"

      # LigandEfficiencies
      if @vue_component == 'LigandEfficiencies'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/target/sections/ligand_efficiencies/#{@model.get('id')}"

      # DocumentBasicInformation
      if @vue_component == 'DocumentBasicInformation'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/document/sections/basic_information/#{@model.get('id')}"

      # RelatedDocuments
      if @vue_component == 'RelatedDocuments'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/document/sections/related_documents/#{@model.get('id')}"

      # DocumentActivityCharts
      if @vue_component == 'DocumentActivityCharts'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/document/sections/activity_charts/#{@model.get('id')}"

      # DocumentAssociatedCompounds
      if @vue_component == 'DocumentAssociatedCompounds'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/document/sections/compound_summary/#{@model.get('id')}"

      # AssayBasicInformation
      if @vue_component == 'AssayBasicInformation'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/assay/sections/basic_information/#{@model.get('id')}"

      # AssayCurationSummary
      if @vue_component == 'AssayCurationSummary'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/assay/sections/curation_summary/#{@model.get('id')}"

      # AssayParameters
      if @vue_component == 'AssayParameters'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/assay/sections/assay_parameters/#{@model.get('id')}"

      # AssayActivityCharts
      if @vue_component == 'AssayActivityCharts'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/assay/sections/activity_charts/#{@model.get('id')}"

      # AssayAssociatedCompounds
      if @vue_component == 'AssayAssociatedCompounds'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/assay/sections/compound_summary/#{@model.get('id')}"

      # CellLineBasicInformation
      if @vue_component == 'CellLineBasicInformation'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/cell_line/sections/basic_information/#{@model.get('id')}"

      # CellLineActivityCharts
      if @vue_component == 'CellLineActivityCharts'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/cell_line/sections/activity_charts/#{@model.get('id')}"

      # CellLineAssociatedCompounds
      if @vue_component == 'CellLineAssociatedCompounds'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/cell_line/sections/compound_summary/#{@model.get('id')}"

      # TissueBasicInformation
      if @vue_component == 'TissueBasicInformation'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/tissue/sections/basic_information/#{@model.get('id')}"

      # TissueActivityCharts
      if @vue_component == 'TissueActivityCharts'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/tissue/sections/activity_charts/#{@model.get('id')}"

      # TissueAssociatedCompounds
      if @vue_component == 'TissueAssociatedCompounds'
        return "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/report_cards/tissue/sections/compound_summary/#{@model.get('id')}"
