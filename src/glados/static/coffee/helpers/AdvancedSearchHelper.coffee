glados.useNameSpace 'glados.helpers',
  AdvancedSearchHelper: class AdvancedSearchHelper

    @showAdvancedSearchModal = ->

      editorModalID = 'modal-AdvancedSearch'
      $editorModal = $("#BCK-GeneratedModalsContainer ##{editorModalID}")

      if $editorModal.length == 0

        $editorModal = ButtonsHelper.generateModalFromTemplate($trigger=undefined,
          'Handlebars-Common-AdvancedSearchModal',
          startingTop=undefined, endingTop=undefined, customID=editorModalID)

        window.addEventListener('message', glados.helpers.AdvancedSearchHelper.handleSearchByIDsMessage, false)

      if not @advancedSearchView?
        @advancedSearchView = new glados.views.SearchResults.AdvancedSearchView
          el: $editorModal

      $editorModal.modal('open')

    @closeAdvancedSearchModal = ->

      editorModalID = 'modal-AdvancedSearch'
      $editorModal = $("#BCK-GeneratedModalsContainer ##{editorModalID}")
      $editorModal.modal('close')

    @handleSearchByIDsMessage = (event) ->

      message = event.data
      if message == 'SEARCH_BY_IDS_SUBMITTED_SUCCESSFULLY'
        glados.helpers.AdvancedSearchHelper.closeAdvancedSearchModal()
