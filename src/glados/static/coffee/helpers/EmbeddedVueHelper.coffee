glados.useNameSpace 'glados.helpers',
  EmbeddedVueHelper: class EmbeddedVueHelper

    @setIframeDynamicResizeListener = ($iframe, childComponentID) ->
      resizeFunction = glados.helpers.EmbeddedVueHelper.generateIframeResizeListener($iframe, childComponentID)
      window.addEventListener('message', resizeFunction, false)

    @generateIframeResizeListener = ($iframe, childComponentID) ->
      handleResizeMsg = (event) ->
        eventData = event.data
        message = eventData.msg

        if message == 'EMBEDDED_VUE_RESIZED'
          idForParent = eventData.data.idForParent
          if idForParent == childComponentID
            contentHeight = eventData.data.height
            $iframe.height("#{contentHeight}px")

        if message == 'OPEN_STRUCTURE_SEARCH'
          sdf = eventData.data.sdf
          glados.helpers.ChemicalEditorHelper.showChemicalEditorModalFromSDF(sdf)

      return handleResizeMsg

    @generateIframeDatasetStateListener = ($iframe, childComponentID) ->
      handleStateUpdate= (event) ->
        eventData = event.data
        message = eventData.msg
        idForParent = eventData.data.idForParent

        if message == 'EMBEDDED_VUE_DATASET_STATE_UPDATED'
          if idForParent == childComponentID
            console.log('idForParent: ', idForParent)
            console.log('message: ', message)
            console.log('idForParent: ', idForParent)
            b64State = eventData.data.b64State
            console.log('b64State: ', b64State)

            glados.helpers.URLHelper.getInstance().updateBrowserURLFromVue('drug_warnings', b64State)
            console.log('state updated!')
      return handleStateUpdate

    @setIframeDatasetStateListener = ($iframe, childComponentID) ->
      handlingFunction = glados.helpers.EmbeddedVueHelper.generateIframeDatasetStateListener($iframe, childComponentID)
      window.addEventListener('message', handlingFunction, false)

