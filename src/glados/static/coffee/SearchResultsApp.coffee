class SearchResultsApp

  # --------------------------------------------------------------------------------------------------------------------
  # Initialization
  # --------------------------------------------------------------------------------------------------------------------

  @init = (selectedESEntity, searchTerm, currentState) ->
    $searchResultsContainer = $('.BCK-SearchResultsContainer')
    new glados.views.SearchResults.SearchResultsView
      el: $searchResultsContainer
      model: SearchModel.getInstance()
      attributes:
        selectedESEntity: selectedESEntity
        state: currentState

    stateObject = if currentState? then JSON.parse(atob(currentState)) else undefined
    SearchModel.getInstance().search(searchTerm, selectedESEntity, stateObject)

  @initSSSearchResults = (searchParams, searchType) ->
    GlobalVariables.SEARCH_TERM = searchParams.search_term

    ssSearchModel = new glados.models.Search.StructureSearchModel
      query_params: searchParams
      search_type: searchType

    $queryContainer = $('.BCK-query-Container')
    if searchType == glados.models.Search.StructureSearchModel.SEARCH_TYPES.SEQUENCE.BLAST
      @initSequenceQueryView($queryContainer, ssSearchModel)
    else
      @initStructureQueryView($queryContainer, ssSearchModel)


    $browserContainer = $('.BCK-BrowserContainer')
    $browserContainer.hide()
    $noResultsDiv = $('.no-results-found')

    listConfig = @getListConfig(searchType)

    thisApp = @
    ssSearchModel.once glados.models.Search.StructureSearchModel.EVENTS.RESULTS_READY, ->
      $browserContainer.show()
      thisApp.initBrowserFromSSResults($browserContainer, $noResultsDiv, listConfig, ssSearchModel)

    ssSearchModel.submitSearch()

  @initSequenceQueryView = ($queryContainer, ssSearchModel) ->
    new glados.views.SearchResults.SequenceQueryView
      el: $queryContainer
      model: ssSearchModel

  @initStructureQueryView = ($queryContainer, ssSearchModel) ->
    new glados.views.SearchResults.StructureQueryView
      el: $queryContainer
      model: ssSearchModel

  @initSearchByIDsResults = (searchJobID) ->
    glados.helpers.AdvancedSearchHelper.closeAdvancedSearchModal()
    searchByIDsModel = new glados.models.Search.SearchByIDsModel
      search_job_id: searchJobID

    $searchProgressIframe = $('#BCK-SearchByIDs-Iframe')
    iframeURL = "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/advanced_search/search_by_ids/status/#{searchJobID}"
    glados.helpers.EmbeddedVueHelper.setIframeDynamicResizeListener(
      $searchProgressIframe,
      'SearchByIDsStatus'
    )

    $searchProgressIframe.attr('src', iframeURL)

    $browserContainer = $('.BCK-BrowserContainer')
    $browserContainer.hide()
    $noResultsDiv = $('.no-results-found')

    thisApp = @
    searchByIDsModel.once glados.models.Search.SearchByIDsModel.EVENTS.RESULTS_READY, ->
      listConfig = thisApp.getSearchByIDsListConfig(searchByIDsModel.get('search_type'))
      $browserContainer.show()
      thisApp.initBrowserFromSearchByIDsResults($browserContainer, $noResultsDiv, listConfig, searchByIDsModel)


    searchByIDsModel.checkSearchStatusPeriodically()


  # --------------------------------------------------------------------------------------------------------------------
  # List Config
  # --------------------------------------------------------------------------------------------------------------------
  @getListConfig = (searchType) ->
    if searchType == glados.models.Search.StructureSearchModel.SEARCH_TYPES.STRUCTURE.SIMILARITY

      listConfig = glados.models.paginatedCollections.Settings.ES_INDEXES_NO_MAIN_SEARCH.COMPOUND_SIMILARITY_MAPS

    else if searchType == glados.models.Search.StructureSearchModel.SEARCH_TYPES.STRUCTURE.SUBSTRUCTURE

      listConfig = glados.models.paginatedCollections.Settings.ES_INDEXES_NO_MAIN_SEARCH.COMPOUND_SUBSTRUCTURE_HIGHLIGHTING

    else if searchType == glados.models.Search.StructureSearchModel.SEARCH_TYPES.STRUCTURE.CONNECTIVITY

      listConfig = glados.models.paginatedCollections.Settings.ES_INDEXES_NO_MAIN_SEARCH.SUBSTRUCTURE_RESULTS_LIST

    else if searchType == glados.models.Search.StructureSearchModel.SEARCH_TYPES.SEQUENCE.BLAST

      listConfig = glados.models.paginatedCollections.Settings.ES_INDEXES_NO_MAIN_SEARCH.TARGET_BLAST_RESULTS

    return listConfig

  @getSearchByIDsListConfig = (searchType) ->
    if searchType == glados.models.Search.SearchByIDsModel.SEARCH_DESTINATIONS.TO_COMPOUNDS

      listConfig = glados.models.paginatedCollections.Settings.ES_INDEXES_NO_MAIN_SEARCH.SEARCH_BY_IDS_TO_COMPOUNDS

    else if searchType == glados.models.Search.SearchByIDsModel.SEARCH_DESTINATIONS.TO_TARGETS

      listConfig = glados.models.paginatedCollections.Settings.ES_INDEXES_NO_MAIN_SEARCH.SEARCH_BY_IDS_TO_TARGETS

    return listConfig

  # --------------------------------------------------------------------------------------------------------------------
  # Router functions
  # --------------------------------------------------------------------------------------------------------------------
  @initSubstructureSearchResults = (searchTerm) ->
    searchParams =
      search_term: searchTerm

    @initSSSearchResults(searchParams, glados.models.Search.StructureSearchModel.SEARCH_TYPES.STRUCTURE.SUBSTRUCTURE)


  @initSimilaritySearchResults = (searchTerm, threshold) ->
    searchParams =
      search_term: searchTerm
      threshold: threshold

    @initSSSearchResults(searchParams, glados.models.Search.StructureSearchModel.SEARCH_TYPES.STRUCTURE.SIMILARITY)


  @initFlexmatchSearchResults = (searchTerm) ->
    searchParams =
      search_term: searchTerm

    @initSSSearchResults(searchParams, glados.models.Search.StructureSearchModel.SEARCH_TYPES.STRUCTURE.CONNECTIVITY)

  @initBLASTSearchResults = (base64Params) ->
    searchParams = JSON.parse(atob(base64Params))

    @initSSSearchResults(searchParams, glados.models.Search.StructureSearchModel.SEARCH_TYPES.SEQUENCE.BLAST)

  @initBrowserFromSSResults = ($browserContainer, $noResultsDiv, customSettings, ssSearchModel) ->
    searchType = ssSearchModel.get('search_type')

    if searchType == glados.models.Search.StructureSearchModel.SEARCH_TYPES.STRUCTURE.CONNECTIVITY
      connectivityQuery = ssSearchModel.get('connectivity_query')

      esCompoundsList = glados.models.paginatedCollections.PaginatedCollectionFactory.getNewESCompoundsList(
        customQuery = undefined, itemsList = undefined , settings = customSettings)
      esCompoundsList.setMeta('sticky_query', connectivityQuery)

    else
      itemsList = ssSearchModel.get('ids_list')
      esCompoundsList = glados.models.paginatedCollections.PaginatedCollectionFactory.getNewESCompoundsList(
        customQuery = undefined, itemsList = itemsList, settings = customSettings, ssSearchModel)

    new glados.views.Browsers.BrowserMenuView
      collection: esCompoundsList
      el: $browserContainer

    esCompoundsList.fetch()

  @initBrowserFromSearchByIDsResults = ($browserContainer, $noResultsDiv, customSettings, searchByIDsModel) ->
    searchType = searchByIDsModel.get('search_type')

    if searchType == glados.models.Search.SearchByIDsModel.SEARCH_DESTINATIONS.TO_COMPOUNDS

      list = glados.models.paginatedCollections.PaginatedCollectionFactory.getNewESCompoundsList(
        customQuery = undefined, itemsList = undefined, settings = customSettings, searchByIDsModel)

    else if searchType == glados.models.Search.SearchByIDsModel.SEARCH_DESTINATIONS.TO_TARGETS

      list = glados.models.paginatedCollections.PaginatedCollectionFactory.getNewESTargetsList(
        customQuery = undefined, settings = customSettings, searchByIDsModel
      )

    new glados.views.Browsers.BrowserMenuView
      collection: list
      el: $browserContainer

    list.fetch()
