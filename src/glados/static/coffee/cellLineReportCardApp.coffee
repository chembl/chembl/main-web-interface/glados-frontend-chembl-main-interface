class CellLineReportCardApp extends glados.ReportCardApp

  # -------------------------------------------------------------
  # Initialisation
  # -------------------------------------------------------------
  @init = ->
    super
    cellLine = CellLineReportCardApp.getCurrentCellLine()

    breadcrumbLinks = [
      {
        label: cellLine.get('id')
        link: CellLine.get_report_card_url(cellLine.get('id'))
      }
    ]
    glados.apps.BreadcrumbApp.setBreadCrumb(breadcrumbLinks)

    CellLineReportCardApp.initBasicInformation()
    CellLineReportCardApp.initActivitySummary()
    CellLineReportCardApp.initAssociatedCompounds()
    cellLine.fetch()

  # -------------------------------------------------------------
  # Singleton
  # -------------------------------------------------------------
  @getCurrentCellLine = ->
    if not @currentCellLine?

      chemblID = glados.Utils.URLS.getCurrentModelChemblID()

      @currentCellLine = new CellLine
        cell_chembl_id: chemblID
      return @currentCellLine

    else return @currentCellLine

  # -------------------------------------------------------------
  # Specific section initialization
  # this is functions only initialize a section of the report card
  # -------------------------------------------------------------
  @initBasicInformation = ->
    cellLine = CellLineReportCardApp.getCurrentCellLine()

    viewConfig =
      embed_section_name: 'cell_line_basic_information'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: cellLine
      el: $('#CBasicInformation')
      config: viewConfig
      section_id: 'BasicInformation'
      section_label: 'Basic Information'
      entity_name: CellLine.prototype.entityName
      report_card_app: @
      vue_component: 'CellLineBasicInformation'


  @initActivitySummary = ->
    cellLine = CellLineReportCardApp.getCurrentCellLine()

    viewConfig =
      embed_section_name: 'bioactivities'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: cellLine
      el: $('#CActivityChartsCard')
      config: viewConfig
      section_id: 'ActivityCharts'
      section_label: 'ActivityCharts'
      entity_name: CellLine.prototype.entityName
      report_card_app: @
      vue_component: 'CellLineActivityCharts'

  @initAssociatedCompounds = (chemblID) ->
    cellLine = CellLineReportCardApp.getCurrentCellLine()

    viewConfig =
      embed_section_name: 'related_compounds'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: cellLine
      el: $('#CLAssociatedCompoundProperties')
      config: viewConfig
      section_id: 'CompoundSummaries'
      section_label: 'Compound Summaries'
      entity_name: CellLine.prototype.entityName
      report_card_app: @
      vue_component: 'CellLineAssociatedCompounds'

  # -------------------------------------------------------------
  # Aggregations
  # -------------------------------------------------------------
  @getAssociatedAssaysAgg = (chemblID) ->
    queryConfig =
      type: glados.models.Aggregations.Aggregation.QueryTypes.QUERY_STRING
      query_string_template: 'cell_chembl_id:{{cell_chembl_id}}'
      template_data:
        cell_chembl_id: 'cell_chembl_id'

    aggsConfig =
      aggs:
        types:
          type: glados.models.Aggregations.Aggregation.AggTypes.TERMS
          field: '_metadata.assay_generated.type_label'
          size: 20
          bucket_links:
            bucket_filter_template: 'cell_chembl_id:{{cell_chembl_id}} ' +
              'AND _metadata.assay_generated.type_label:("{{bucket_key}}"' +
              '{{#each extra_buckets}} OR "{{this}}"{{/each}})'
            template_data:
              cell_chembl_id: 'cell_chembl_id'
              bucket_key: 'BUCKET.key'
              extra_buckets: 'EXTRA_BUCKETS.key'

            link_generator: Assay.getAssaysListURL

    associatedAssays = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.ASSAY_INDEX_URL
      query_config: queryConfig
      cell_chembl_id: chemblID
      aggs_config: aggsConfig

    return associatedAssays

  @getAssociatedBioactivitiesAgg = (chemblID) ->
    queryConfig =
      type: glados.models.Aggregations.Aggregation.QueryTypes.QUERY_STRING
      query_string_template: '_metadata.assay_data.cell_chembl_id:{{cell_chembl_id}}'
      template_data:
        cell_chembl_id: 'cell_chembl_id'

    aggsConfig =
      aggs:
        types:
          type: glados.models.Aggregations.Aggregation.AggTypes.TERMS
          field: 'standard_type'
          size: 20
          bucket_links:
            bucket_filter_template: '_metadata.assay_data.cell_chembl_id:{{cell_chembl_id}} ' +
              'AND standard_type:("{{bucket_key}}"' +
              '{{#each extra_buckets}} OR "{{this}}"{{/each}})'
            template_data:
              cell_chembl_id: 'cell_chembl_id'
              bucket_key: 'BUCKET.key'
              extra_buckets: 'EXTRA_BUCKETS.key'

            link_generator: Activity.getActivitiesListURL

    bioactivities = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.ACTIVITY_INDEX_URL
      query_config: queryConfig
      cell_chembl_id: chemblID
      aggs_config: aggsConfig

    return bioactivities

  @getAssociatedCompoundsAgg = (chemblID, minCols = 1, maxCols = 20, defaultCols = 10) ->
    queryConfig =
      type: glados.models.Aggregations.Aggregation.QueryTypes.MULTIMATCH
      queryValueField: 'cell_chembl_id'
      fields: ['_metadata.related_cell_lines.all_chembl_ids']

    aggsConfig =
      aggs:
        x_axis_agg:
          field: 'molecule_properties.full_mwt'
          type: glados.models.Aggregations.Aggregation.AggTypes.RANGE
          min_columns: minCols
          max_columns: maxCols
          num_columns: defaultCols
          bucket_links:
            bucket_filter_template: '_metadata.related_cell_lines.all_chembl_ids:{{cell_chembl_id}} ' +
              'AND molecule_properties.full_mwt:(>={{min_val}} AND <={{max_val}})'
            template_data:
              cell_chembl_id: 'cell_chembl_id'
              min_val: 'BUCKET.from'
              max_val: 'BUCKETS.to'
            link_generator: Compound.getCompoundsListURL

    associatedCompounds = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.COMPOUND_INDEX_URL
      query_config: queryConfig
      cell_chembl_id: chemblID
      aggs_config: aggsConfig

    return associatedCompounds

  # --------------------------------------------------------------------------------------------------------------------
  # Mini histograms
  # --------------------------------------------------------------------------------------------------------------------
  @initMiniActivitiesHistogram = ($containerElem, chemblID) ->
    bioactivities = CellLineReportCardApp.getAssociatedBioactivitiesAgg(chemblID)

    stdTypeProp = glados.models.visualisation.PropertiesFactory.getPropertyConfigFor('Activity', 'STANDARD_TYPE',
      withColourScale = true)

    barsColourScale = stdTypeProp.colourScale

    config =
      max_categories: 8
      bars_colour_scale: barsColourScale
      fixed_bar_width: true
      hide_title: false
      x_axis_prop_name: 'types'
      properties:
        std_type: stdTypeProp
      initial_property_x: 'std_type'

    new glados.views.Visualisation.HistogramView
      model: bioactivities
      el: $containerElem
      config: config

    bioactivities.fetch()

  @initMiniCompoundsHistogram = ($containerElem, chemblID) ->
    associatedCompounds = CellLineReportCardApp.getAssociatedCompoundsAgg(chemblID, minCols = 8,
      maxCols = 8, defaultCols = 8)

    config =
      max_categories: 8
      fixed_bar_width: true
      hide_title: false
      x_axis_prop_name: 'x_axis_agg'
      properties:
        mwt: glados.models.visualisation.PropertiesFactory.getPropertyConfigFor('Compound', 'FULL_MWT')
      initial_property_x: 'mwt'

    new glados.views.Visualisation.HistogramView
      model: associatedCompounds
      el: $containerElem
      config: config

    associatedCompounds.fetch()

  @initMiniHistogramFromFunctionLink = ->
    $clickedLink = $(@)

    [paramsList, constantParamsList, $containerElem] = \
      glados.views.PaginatedViews.PaginatedTable.prepareAndGetParamsFromFunctionLinkCell($clickedLink)

    histogramType = constantParamsList[0]
    chemblID = paramsList[0]

    if histogramType == 'activities'
      CellLineReportCardApp.initMiniActivitiesHistogram($containerElem, chemblID)
    else if histogramType == 'compounds'
      CellLineReportCardApp.initMiniCompoundsHistogram($containerElem, chemblID)
