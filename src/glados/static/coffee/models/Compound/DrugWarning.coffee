glados.useNameSpace 'glados.models.Compound',
  DrugWarning: Backbone.Model.extend

    entityName: 'Drug Warning'
    entityNamePlural: 'Drug Warnings'

glados.models.Compound.DrugWarning.getListURL = (filter, isFullState=false, fragmentOnly=false) ->

  return "#{glados.Settings.VUE_BROWSER_PAGES_BASE_URL}/drug_warnings"
