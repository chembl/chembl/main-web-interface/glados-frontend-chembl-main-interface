glados.useNameSpace 'glados.models.paginatedCollections',

  PaginatedCollectionBase: Backbone.Collection.extend

    initialize: ->

      @setInitialFetchingState()
      @setInitialSearchState()
      @setInitialConfigState()
      if @islinkToOtherEntitiesEnabled()
        @on glados.Events.Collections.SELECTION_UPDATED, @resetLinkToOtherEntitiesCache, @

        @on glados.models.paginatedCollections.PaginatedCollectionBase.EVENTS.FACETS_FETCHING_STATE_CHANGED,
        @resetLinkToOtherEntitiesCache, @

    islinkToOtherEntitiesEnabled: ->

      linksToOtherEntities = @getMeta('links_to_other_entities')
      if not linksToOtherEntities?
        return false
      if linksToOtherEntities.length == 0
        return false

      return true

    getTotalRecords: ->
      totalRecords = @getMeta('total_records')
      totalRecords ?= 0
      return totalRecords

    getLinkToListFunction: -> @getMeta('browse_list_url')
    # ------------------------------------------------------------------------------------------------------------------
    # Downloads
    # ------------------------------------------------------------------------------------------------------------------
    downloadIsValidAndReady: ->

      if not @allResults?
        return false

      if not @DOWNLOADED_ITEMS_ARE_VALID
        return false

      for result in @allResults
        if not result?
          return false

      return true

    # ------------------------------------------------------------------------------------------------------------------
    # Utils
    # ------------------------------------------------------------------------------------------------------------------
    getModelEntityName: -> @getMeta('model').prototype.entityName
    getModelEntityNameForJoin: -> @getMeta('model').prototype.entityNameForJoin
    getIDProperty: -> @getMeta('id_column').comparator
    # ------------------------------------------------------------------------------------------------------------------
    # Link to all activities and other entities
    # ------------------------------------------------------------------------------------------------------------------
    DESTINATION_ENTITY_NAMES_FOR_JOIN:
      "#{Activity.prototype.entityName}": 'CHEMBL_ACTIVITIES'
      "#{Compound.prototype.entityName}": 'CHEMBL_COMPOUNDS'
      "#{Target.prototype.entityName}": 'CHEMBL_TARGETS'
      "#{Assay.prototype.entityName}": 'CHEMBL_ASSAYS'
      "#{Document.prototype.entityName}": 'CHEMBL_DOCUMENTS'
      "#{CellLine.prototype.entityName}": 'CHEMBL_CELL_LINES'
      "#{glados.models.Tissue.prototype.entityName}": 'CHEMBL_TISSUES'
      "#{glados.models.Compound.Drug.prototype.browseLinkEntityName}": 'CHEMBL_DRUGS'
      "#{glados.models.Compound.DrugIndication.prototype.entityName}": 'CHEMBL_DRUG_INDICATIONS'
      "#{glados.models.Compound.MechanismOfAction.prototype.entityName}": 'CHEMBL_DRUG_MECHANISMS'



    thereAreTooManyItemsForActivitiesLink: ->

      numSelectedItems = @getNumberOfSelectedItems()
      numItemsForLink = if numSelectedItems == 0 then @getTotalRecords() else numSelectedItems
      if numItemsForLink >= glados.Settings.VIEW_SELECTION_THRESHOLDS.Heatmap[1]
        return true
      return false

    resetLinkToOtherEntitiesCache: ->

    getJoinDestinationEntityBrowserStateTemplate: ->

      destinationEntityBrowserStateTemplate = glados.Settings.ENTITY_BROWSERS_URL_GENERATOR
        fragment_only: false
        entity: '<BROWSER_NAME>'
        filter: '<GENERATED_STATE>'
        is_full_state: true

      destinationEntityBrowserStateTemplate = destinationEntityBrowserStateTemplate
          .replace(/&lt;/g, '<').replace(/&gt;/g, '>')

      return destinationEntityBrowserStateTemplate

    getJoinSelectionDescription: ->

      if @allItemsAreUnselected()
        return {
          selectionMode: 'allItemsExcept',
          exceptions: []
        }
      else
        return {
          selectionMode: if @getMeta('all_items_selected') then 'allItemsExcept' else 'noItemsExcept'
          exceptions: Object.keys(@getMeta('selection_exceptions'))
        }

    getLinkToRelatedEntitiesPromise: (destinationEntityName) ->

      # shorten the current url to later create a link to this state from the join destination
      urlToShorten = window.location.href.match(glados.Settings.SHORTENING_MATCH_REPEXG)[0]
      paramsDict =
        long_url: urlToShorten

      linkPromise = jQuery.Deferred()

      thisCollection = @
      shortenURL = $.post(glados.Settings.SHORTEN_URLS_ENDPOINT, paramsDict)
      shortenURL.then (data) ->

        ssSearchModel = thisCollection.getMeta('sssearch_model')
        paramsDict = {
          'destination_entity_browser_state_template': thisCollection.getJoinDestinationEntityBrowserStateTemplate()
          'previous_hash': data.hash,
          'entity_from': thisCollection.getModelEntityNameForJoin(),
          'entity_to': thisCollection.DESTINATION_ENTITY_NAMES_FOR_JOIN[destinationEntityName]
          'es_query': thisCollection.getESRequestData().es_query,
          'selection_description': JSON.stringify(thisCollection.getJoinSelectionDescription())
          context_obj: if ssSearchModel? then JSON.stringify(ssSearchModel.getContextObj()) else undefined
        }

        joinEntities = $.post(glados.Settings.LINK_TO_RELATED_ITEMS_ENDPOINT, paramsDict)
        joinEntities.then (data) ->
          linkPromise.resolve(data)

      return linkPromise

    # This gives a filter from a source entity to a destination entity. For example if you want from the compounds
    # to get the related activities you need ['Compound']['Activity']
    ENTITY_NAME_TO_FILTER_GENERATOR:
      "#{Compound.prototype.entityName}":
        "#{Activity.prototype.entityName}":\
          Handlebars.compile('molecule_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
        "#{glados.models.Compound.Drug.prototype.entityName}":\
          Handlebars.compile('molecule_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
        "#{glados.models.Compound.MechanismOfAction.prototype.entityName}":\
          Handlebars.compile(
            'mechanism_of_action.molecule_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})'+
            ' OR '+
            'mechanism_of_action.parent_molecule_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
        "#{glados.models.Compound.DrugIndication.prototype.entityName}":\
          Handlebars.compile(
            'drug_indication.molecule_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})'+
            ' OR '+
            'drug_indication.parent_molecule_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
      "#{glados.models.Compound.Drug.prototype.entityName}":
        "#{Activity.prototype.entityName}":\
          Handlebars.compile('molecule_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
        "#{glados.models.Compound.Drug.prototype.entityName}":\
          Handlebars.compile('molecule_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
        "#{glados.models.Compound.MechanismOfAction.prototype.entityName}":\
          Handlebars.compile(
            'mechanism_of_action.molecule_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})'+
            ' OR '+
            'mechanism_of_action.parent_molecule_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
        "#{glados.models.Compound.DrugIndication.prototype.entityName}":\
          Handlebars.compile(
            'drug_indication.molecule_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})'+
            ' OR '+
            'drug_indication.parent_molecule_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
      "#{Target.prototype.entityName}":
        "#{Activity.prototype.entityName}":\
          Handlebars.compile('target_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
        "#{glados.models.Compound.MechanismOfAction.prototype.entityName}":\
          Handlebars.compile('target.target_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
      "#{Document.prototype.entityName}":
        "#{Activity.prototype.entityName}":\
          Handlebars.compile('document_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
      "#{Assay.prototype.entityName}":
        "#{Activity.prototype.entityName}":\
          Handlebars.compile('assay_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
      "#{CellLine.prototype.entityName}":
        "#{Activity.prototype.entityName}":\
          Handlebars.compile('_metadata.assay_data.cell_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
      "#{glados.models.Tissue.prototype.entityName}":
        "#{Activity.prototype.entityName}":\
          Handlebars.compile('_metadata.assay_data.tissue_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
      "#{Activity.prototype.entityName}":
        "#{Compound.prototype.entityName}":
          Handlebars.compile('molecule_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
        "#{Target.prototype.entityName}":
          Handlebars.compile('target_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
        "#{Assay.prototype.entityName}":
          Handlebars.compile('assay_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
        "#{Document.prototype.entityName}":
          Handlebars.compile('document_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
      "#{glados.models.Compound.MechanismOfAction.prototype.entityName}":
        "#{glados.models.Compound.Drug.prototype.browseLinkEntityName}":
          join_property_comparator: 'parent_molecule.molecule_chembl_id'
          search_filter:\
            Handlebars.compile('molecule_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
        "#{Compound.prototype.entityName}":
          join_property_comparator: 'parent_molecule.molecule_chembl_id'
          search_filter:\
            Handlebars.compile(
              'molecule_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}}) OR '+
              'molecule_hierarchy.parent_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})'
            )
        "#{Target.prototype.entityName}":
          join_property_comparator: 'target.target_chembl_id'
          search_filter:\
            Handlebars.compile(
              'target_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})'
            )
      "#{glados.models.Compound.DrugIndication.prototype.entityName}":
        "#{glados.models.Compound.Drug.prototype.browseLinkEntityName}":
          join_property_comparator: 'parent_molecule.molecule_chembl_id'
          search_filter:\
            Handlebars.compile('molecule_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})')
        "#{Compound.prototype.entityName}":
          join_property_comparator: 'parent_molecule.molecule_chembl_id'
          search_filter:\
            Handlebars.compile(
              'molecule_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}}) OR '+
              'molecule_hierarchy.parent_chembl_id:({{#each ids}}"{{this}}"{{#unless @last}} OR {{/unless}}{{/each}})'
            )

    # ------------------------------------------------------------------------------------------------------------------
    # Fetching state handling
    # ------------------------------------------------------------------------------------------------------------------
    usesFacets: ->
      if not @getMeta('facets_groups')?
        return false
      if Object.keys(@getMeta('facets_groups')).length == 0
        return false
      return true

    getItemsFetchingState: -> @getMeta('items_fetching_state')
    getFacetsFetchingState: -> @getMeta('facets_fetching_state')
    setItemsFetchingState: (newFetchingState) ->
      @setMeta('items_fetching_state', newFetchingState)
      @trigger(glados.models.paginatedCollections.PaginatedCollectionBase.EVENTS.ITEMS_FETCHING_STATE_CHANGED)
    setFacetsFetchingState: (newFetchingState) ->
      @setMeta('facets_fetching_state', newFetchingState)
      @trigger(glados.models.paginatedCollections.PaginatedCollectionBase.EVENTS.FACETS_FETCHING_STATE_CHANGED)
    setInitialFetchingState: ->
      @setMeta('items_fetching_state',
        glados.models.paginatedCollections.PaginatedCollectionBase.ITEMS_FETCHING_STATES.INITIAL_STATE)
      @setMeta('facets_fetching_state',
        glados.models.paginatedCollections.PaginatedCollectionBase.FACETS_FETCHING_STATES.INITIAL_STATE)

    itemsAreReady: ->

      itemsAreReady = @getMeta('items_fetching_state') == \
      glados.models.paginatedCollections.PaginatedCollectionBase.ITEMS_FETCHING_STATES.ITEMS_READY
      return itemsAreReady

    facetsAreReady: ->

      facetsAreReady = @getMeta('facets_fetching_state') == \
      glados.models.paginatedCollections.PaginatedCollectionBase.FACETS_FETCHING_STATES.FACETS_READY
      return facetsAreReady

    isReady: -> @itemsAreReady() and @facetsAreReady()

    facetsAreInInitalState: -> @getMeta('facets_fetching_state') == \
      glados.models.paginatedCollections.PaginatedCollectionBase.FACETS_FETCHING_STATES.INITIAL_STATE

    itemsAreInInitalState: -> @getMeta('items_fetching_state') == \
      glados.models.paginatedCollections.PaginatedCollectionBase.ITEMS_FETCHING_STATES.INITIAL_STATE

    # ------------------------------------------------------------------------------------------------------------------
    # Searching state handling
    # ------------------------------------------------------------------------------------------------------------------
    getSearchState: -> @getMeta('search_state')
    setInitialSearchState: -> @setMeta('search_state',
      glados.models.paginatedCollections.PaginatedCollectionBase.SEARCHING_STATES.SEARCH_UNDEFINED)
    setSearchState: (newState) ->
      oldState = @getSearchState()
      if oldState != newState
        @setMeta('search_state', newState)
        @trigger(glados.models.paginatedCollections.PaginatedCollectionBase.EVENTS.SEARCH_STATE_CHANGED)
    searchQueryIsSet: -> @getSearchState() ==\
      glados.models.paginatedCollections.PaginatedCollectionBase.SEARCHING_STATES.SEARCH_QUERY_SET
    searchIsReady: -> @getMeta('search_state') ==\
      glados.models.paginatedCollections.PaginatedCollectionBase.SEARCHING_STATES.SEARCH_IS_READY

    # ------------------------------------------------------------------------------------------------------------------
    # Configuration state handling
    # ------------------------------------------------------------------------------------------------------------------
    getConfigState: -> @getMeta('config_state')
    setInitialConfigState: -> @setMeta('config_state',
      glados.models.paginatedCollections.PaginatedCollectionBase.CONFIGURATION_FETCHING_STATES.INITIAL_STATE)
    setConfigState: (newState) ->
      oldState = @getConfigState()
      if oldState != newState
        @setMeta('config_state', newState)
        @trigger(glados.models.paginatedCollections.PaginatedCollectionBase.EVENTS.CONFIG_FETCHING_STATE_CHANGED)
    configIsReady: -> @getConfigState() ==\
      glados.models.paginatedCollections.PaginatedCollectionBase.CONFIGURATION_FETCHING_STATES.CONFIGURATION_READY

    # ------------------------------------------------------------------------------------------------------------------
    # Facets Configuration state handling
    # ------------------------------------------------------------------------------------------------------------------
    getFacetsConfigState: -> @getMeta('facets_config_state')
    setInitialFacetsConfigState: -> @setMeta('facets_config_state',
      glados.models.paginatedCollections.PaginatedCollectionBase.FACETS_CONFIGURATION_FETCHING_STATES.INITIAL_STATE)
    setFacetsConfigState: (newState) ->
      oldState = @getFacetsConfigState()
      if oldState != newState
        @setMeta('facets_config_state', newState)
        @trigger(glados.models.paginatedCollections.PaginatedCollectionBase.EVENTS.FACETS_CONFIG_FETCHING_STATE_CHANGED)
    facetsConfigIsReady: -> @getFacetsConfigState() ==\
      glados.models.paginatedCollections.PaginatedCollectionBase.FACETS_CONFIGURATION_FETCHING_STATES.CONFIGURATION_READY
    # ------------------------------------------------------------------------------------------------------------------ 
    # Sleep/Awake states
    # ------------------------------------------------------------------------------------------------------------------
    getAwakenState: -> @getMeta('awaken_state')
    isSleeping: -> @getMeta('awaken_state') ==\
      glados.models.paginatedCollections.PaginatedCollectionBase.AWAKEN_STATES.SLEEPING
    isAwaken: -> @getMeta('awaken_state') ==\
      glados.models.paginatedCollections.PaginatedCollectionBase.AWAKEN_STATES.AWAKEN

    setAwakenState: (newState) ->
      oldState = @getAwakenState()
      if oldState != newState
        @setMeta('awaken_state', newState)
        @trigger(glados.models.paginatedCollections.PaginatedCollectionBase.EVENTS.AWAKE_STATE_CHANGED)
    wakeUp: ->
      @setAwakenState(glados.models.paginatedCollections.PaginatedCollectionBase.AWAKEN_STATES.AWAKEN)
    sleep: ->
      @setAwakenState(glados.models.paginatedCollections.PaginatedCollectionBase.AWAKEN_STATES.SLEEPING)

    awakenStateIsUnknown: -> not @getAwakenState()?

    doFetchWhenAwaken: ->

      if @isAwaken()
        @fetch()
        return

      fetchIfAwaken = ->

        if @isAwaken()
          @off glados.models.paginatedCollections.PaginatedCollectionBase.EVENTS.AWAKE_STATE_CHANGED, fetchIfAwaken, @
          @fetch()

      @once glados.models.paginatedCollections.PaginatedCollectionBase.EVENTS.AWAKE_STATE_CHANGED, fetchIfAwaken, @

    setTextFilter: -> # See ESCollectionExtFilterFunctions
    getTextFilter: ->
    clearTextFilter: ->


glados.models.paginatedCollections.PaginatedCollectionBase.EVENTS =
  ITEMS_FETCHING_STATE_CHANGED: 'ITEMS_FETCHING_STATE_CHANGED'
  FACETS_FETCHING_STATE_CHANGED: 'FACETS_FETCHING_STATE_CHANGED'
  SEARCH_STATE_CHANGED: 'SEARCH_STATE_CHANGED'
  AWAKE_STATE_CHANGED: 'AWAKE_STATE_CHANGED'
  STATE_OBJECT_CHANGED: 'STATE_OBJECT_CHANGED'
  CONFIG_FETCHING_STATE_CHANGED: 'CONFIG_FETCHING_STATE_CHANGED'
  FACETS_CONFIG_FETCHING_STATE_CHANGED: 'FACETS_CONFIG_FETCHING_STATE_CHANGED'
  SHOULD_RESET_PAGE_NUMBER: 'SHOULD_RESET_PAGE_NUMBER'

glados.models.paginatedCollections.PaginatedCollectionBase.ITEMS_FETCHING_STATES =
  INITIAL_STATE: 'INITIAL_STATE'
  FETCHING_ITEMS: 'FETCHING_ITEMS'
  ITEMS_READY: 'ITEMS_READY'

glados.models.paginatedCollections.PaginatedCollectionBase.FACETS_FETCHING_STATES =
  INITIAL_STATE: 'INITIAL_STATE'
  FETCHING_FACETS: 'FETCHING_FACETS'
  FACETS_READY: 'FACETS_READY'

glados.models.paginatedCollections.PaginatedCollectionBase.CONFIGURATION_FETCHING_STATES =
  INITIAL_STATE: 'INITIAL_STATE'
  FETCHING_CONFIGURATION: 'FETCHING_CONFIGURATION'
  CONFIGURATION_READY: 'CONFIGURATION_READY'

glados.models.paginatedCollections.PaginatedCollectionBase.FACETS_CONFIGURATION_FETCHING_STATES =
  INITIAL_STATE: 'INITIAL_STATE'
  FETCHING_CONFIGURATION: 'FETCHING_FACETS_CONFIGURATION'
  CONFIGURATION_READY: 'FACET_CONFIGURATION_READY'

glados.models.paginatedCollections.PaginatedCollectionBase.SEARCHING_STATES =
  SEARCH_UNDEFINED: 'SEARCH_UNDEFINED'
  SEARCH_QUERY_SET: 'SEARCH_QUERY_SET' #this means that the esQuery is set and it is ready to be fetched, but fetching
  # has not been done
  SEARCH_IS_READY: 'SEARCH_IS_READY' # the fetch has been done, and the data has been received and parsed.

glados.models.paginatedCollections.PaginatedCollectionBase.AWAKEN_STATES =
  SLEEPING: 'SLEEPING'
  AWAKEN: 'AWAKEN'