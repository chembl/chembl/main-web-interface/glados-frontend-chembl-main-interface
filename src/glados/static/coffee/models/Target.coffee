Target = Backbone.Model.extend(DownloadModelOrCollectionExt).extend

  entityName: 'Target'
  entityNamePlural: 'Targets'
  entityNameForJoin: 'CHEMBL_TARGETS'
  idAttribute: 'target_chembl_id'
  defaults:
    fetch_from_elastic: true
  initialize: ->
    @initURL()

  initURL: ->
    id = @get('id')
    id ?= @get('target_chembl_id')
    @set('id', id)
    @set('target_chembl_id', id)

    if @get('fetch_from_elastic')
      @url = "#{glados.Settings.ES_PROXY_API_BASE_URL}/es_data/get_es_document/#{Target.ES_INDEX}/#{id}"
    else
      @url = glados.Settings.WS_BASE_URL + 'target/' + id + '.json'

  getProteinTargetClassification: (response) ->
    console.log('getProteinTargetClassification', response)

    all_classifications = []
    raw_classifications = glados.Utils.getNestedValue(response,
      '_source._metadata.protein_classification')

    # process each of the classifications
    for raw_classification in raw_classifications
      new_classification = []
      levels_stack = []
      # iterate each level from 1 to 6
      for level in [1..6]
        level_id_key = "l#{level}"
        level_id = raw_classification[level_id_key]
        if not level_id?
          break

        levels_stack.push(level_id)
        this_level_stack = levels_stack.slice(0, level)

        # builds the querystring for the level which is _metadata.protein_classification.l1:("<level_id1>")
        # AND _metadata.protein_classification.l2:("<level_id2>") ...
        querystring = ''
        for classification, index in this_level_stack
          querystring += "_metadata.protein_classification.l#{index + 1}:\"#{classification}\""
          if index < this_level_stack.length - 1
            querystring += ' AND '

        browser_url = Target.getTargetsListURL(querystring)
        level = {
          level_id: level_id
          browser_url: browser_url,
        }
        new_classification.push(level)

      all_classifications.push(new_classification)

    return all_classifications

# --------------------------------------------------------------------------------------------------------------------
# Parsing
# --------------------------------------------------------------------------------------------------------------------
  parse: (response) ->

# get data when it comes from elastic
    if response._source?
      objData = response._source
    else
      objData = response

    objData.report_card_url = Target.get_report_card_url(objData.target_chembl_id)
    filterForActivities = 'target_chembl_id:' + objData.target_chembl_id
    objData.activities_url = Activity.getActivitiesListURL(filterForActivities)
    filterForCompounds = '_metadata.related_targets.all_chembl_ids:' + objData.target_chembl_id
    objData.compounds_url = Compound.getCompoundsListURL(filterForCompounds)

    objData.parsed_protein_classification = @getProteinTargetClassification(response)
    console.log('PARSE')
    console.log('parsed_protein_classification', objData.parsed_protein_classification)

    @parseXrefs(objData)
    return objData

  parseXrefs: (objData) ->
    originalRefs = objData.cross_references
    refsIndex = _.indexBy(originalRefs, (item) -> "#{item.xref_src}-#{item.xref_id}")
    targetComponents = objData.target_components

    addXrefToOriginalRefs = (xref, refsIndex, originalRefs) ->
      refIdentifier = "#{xref.xref_src_db}-#{xref.xref_id}"
      xref.xref_src = xref.xref_src_db
      # just in case to avoid duplicates
      if not refsIndex[refIdentifier]?
        originalRefs.push xref
        refsIndex[refIdentifier] = xref

    for component in targetComponents
      componentXrefs = component.target_component_xrefs

      if not componentXrefs?
        continue

      for xref in componentXrefs

        addXrefToOriginalRefs(xref, refsIndex, originalRefs)
        # check if it needs to duplicate it
        if xref.xref_src == 'EnsemblGene'

          newXref = $.extend {}, xref,
            xref_src_db: 'Human Protein Atlas'
            xref_src_url: 'http://www.proteinatlas.org/'
            xref_url: "http://www.proteinatlas.org/#{xref.xref_id}"

          addXrefToOriginalRefs(newXref, refsIndex, originalRefs)

          newXref = $.extend {}, xref,
            xref_src_db: 'Open Targets'
            xref_src_url: 'https://www.targetvalidation.org/'
            xref_url: "https://www.targetvalidation.org/target/#{xref.xref_id}/associations"

          addXrefToOriginalRefs(newXref, refsIndex, originalRefs)

        if xref.xref_src == 'PDBe'

          newXref = $.extend {}, xref,
            xref_src_db: 'CREDO'
            xref_src_url: 'http://marid.bioc.cam.ac.uk/credo'
            xref_url: "http://marid.bioc.cam.ac.uk/credo/structures/#{xref.xref_id}"

          addXrefToOriginalRefs(newXref, refsIndex, originalRefs)

  fetchFromAssayChemblID: ->
    assayUrl = glados.Settings.WS_BASE_URL + 'assay/' + @get('assay_chembl_id') + '.json'

    thisTarget = @
    $.get(assayUrl).done((response) ->
      thisTarget.set('target_chembl_id', response.target_chembl_id)
      thisTarget.initURL()
      thisTarget.fetch()
    ).fail(
      () -> console.log('failed!')
    )

# Constant definition for ReportCardEntity model functionalities
_.extend(Target, glados.models.base.ReportCardEntity)
Target.color = 'lime'
Target.reportCardPath = 'target_report_card/'

Target.ES_INDEX = 'chembl_target'
Target.INDEX_NAME = Target.ES_INDEX

Target.PROPERTIES_VISUAL_CONFIG = {
  'target_chembl_id': {
    link_base: 'report_card_url'
  },
  'pref_name': {
    custom_field_template: '<i>{{val}}</i>'
  },
  'uniprot_accessions': {
    parse_function: (components) ->
      if not components?
        return glados.Utils.DEFAULT_NULL_VALUE_LABEL

      if components.length == 0
        return glados.Utils.DEFAULT_NULL_VALUE_LABEL

      return (comp.accession for comp in components).join(', ')
    link_function: (components) ->
      'http://www.uniprot.org/uniprot/?query=' + ('accession:' + comp.accession for comp in components).join('+OR+')
  }
  '_metadata.related_compounds.count': {
    link_base: 'compounds_url'
    on_click: TargetReportCardApp.initMiniHistogramFromFunctionLink
    function_constant_parameters: ['compounds']
    function_parameters: ['target_chembl_id']
# to help bind the link to the function, it could be necessary to always use the key of the columns descriptions
# or probably not, depending on how this evolves
    function_key: 'num_compounds'
    function_link: true
    execute_on_render: true
    format_class: 'number-cell-center'
  }
  '_metadata.related_activities.count': {
    link_base: 'activities_url'
    on_click: TargetReportCardApp.initMiniHistogramFromFunctionLink
    function_parameters: ['target_chembl_id']
    function_constant_parameters: ['activities']
# to help bind the link to the function, it could be necessary to always use the key of the columns descriptions
# or probably not, depending on how this evolves
    function_key: 'bioactivities'
    function_link: true
    execute_on_render: true
    format_class: 'number-cell-center'
  }
  'best_expectation': {
    'show': true
    'comparator': '_context.best_expectation'
    'sort_disabled': false
    'is_sorting': 0
    'sort_class': 'fa-sort'
    'is_contextual': true
  }
  'best_positives': {
    'show': true
    'comparator': '_context.best_positives'
    'sort_disabled': false
    'is_sorting': 0
    'sort_class': 'fa-sort'
    'is_contextual': true
  }
  'best_identities': {
    'show': true
    'comparator': '_context.best_identities'
    'sort_disabled': false
    'is_sorting': 0
    'sort_class': 'fa-sort'
    'is_contextual': true
  }
  'best_score_bits': {
    'show': true
    'comparator': '_context.best_score_bits'
    'sort_disabled': false
    'is_sorting': 0
    'sort_class': 'fa-sort'
    'is_contextual': true
  }
  'best_score': {
    'show': true
    'comparator': '_context.best_score'
    'sort_disabled': false
    'is_sorting': 0
    'sort_class': 'fa-sort'
    'is_contextual': true
  }
  'length': {
    'show': true
    'name_to_show': 'Length'
    'comparator': '_context.length'
    'sort_disabled': false
    'is_sorting': 0
    'sort_class': 'fa-sort'
    'is_contextual': true
  }
}

Target.COLUMNS = {
  CHEMBL_ID:
    aggregatable: true
    comparator: "target_chembl_id"
    hide_label: true
    id: "target_chembl_id"
    is_sorting: 0
    link_base: "report_card_url"
    name_to_show: "ChEMBL ID"
    name_to_show_short: "ChEMBL ID"
    show: true
    sort_class: "fa-sort"
    sort_disabled: false

}

Target.ID_COLUMN = Target.COLUMNS.CHEMBL_ID

Target.MINI_REPORT_CARD =
  LOADING_TEMPLATE: 'Handlebars-Common-MiniRepCardPreloader'
  TEMPLATE: 'Handlebars-Common-MiniReportCard'

Target.getTargetsListURL = (filter, isFullState=false, fragmentOnly=false, description = undefined) ->

  if isFullState or fragmentOnly
    if isFullState
      filter = btoa(JSON.stringify(filter))
    return glados.Settings.ENTITY_BROWSERS_URL_GENERATOR
      fragment_only: fragmentOnly
      entity: 'targets'
      filter: encodeURIComponent(filter) unless not filter?
      is_full_state: isFullState

  stateString = ''
  if filter?
    # the replace is to avoid issues with the / even after encoding
    # the replace is to avoid issues with the / even after encoding
    superSafeFilter = filter.replace('/','__SLASH__')
    stateString = encodeURIComponent("QUERYSTRING:#{superSafeFilter}")

  descriptionString = ''
  if description?
    descriptionString = "?description=#{encodeURIComponent(description)}"

  return "#{glados.Settings.VUE_BROWSER_PAGES_BASE_URL}/targets/#{stateString}#{descriptionString}"
