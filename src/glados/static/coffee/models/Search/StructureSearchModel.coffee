glados.useNameSpace 'glados.models.Search',

# This model handles the communication with the server for structure searches
  StructureSearchModel: Backbone.Model.extend

    initialize: ->
      @setState(glados.models.Search.StructureSearchModel.STATES.INITIAL_STATE)

    submitSearch: ->
      searchType = @get('search_type')

      # Similarity search does not actually submit a job anymore, does it directly
      if searchType == glados.models.Search.StructureSearchModel.SEARCH_TYPES.STRUCTURE.SIMILARITY
        return @setState(glados.models.Search.StructureSearchModel.STATES.FINISHED)

      # Connectivity search does not actually submit a job anymore either, does it directly
      if searchType == glados.models.Search.StructureSearchModel.SEARCH_TYPES.STRUCTURE.CONNECTIVITY
        return @loadQueryForConnectivitySearch()


      if searchType == glados.models.Search.StructureSearchModel.SEARCH_TYPES.SEQUENCE.BLAST
        paramsDict = @getSequenceSearchParamsDict()
        submissionURL = glados.Settings.SUBMIT_SEQUENCE_SEARCH_URL
      else
        paramsDict = @getStructureSearchParamsDict()
        submissionURL = glados.Settings.SUBMIT_STRUCTURE_SEARCH_URL

      submitPromise = $.post(submissionURL, paramsDict)

      thisModel = @
      submitPromise.then (data) ->
        thisModel.set('search_id', data.job_id)
        thisModel.setState(glados.models.Search.StructureSearchModel.STATES.SEARCH_QUEUED)

      submitPromise.fail (jqXHR) ->
        errorMSG = if jqXHR.status == 500 then jqXHR.responseText else jqXHR.statusText
        thisModel.set('error_message', errorMSG)
        thisModel.setState(glados.models.Search.StructureSearchModel.STATES.ERROR_STATE)

    getStructureSearchParamsDict: ->
      queryParams = @get('query_params')

      paramsDict =
        search_type: @get('search_type')
        search_term: queryParams.search_term
        threshold: queryParams.threshold
        dl__ignore_cache: false

      return paramsDict

    getSequenceSearchParamsDict: ->
      queryParams = @get('query_params')

      paramsDict = queryParams
      paramsDict.dl__ignore_cache = true

      return paramsDict

    oldSubmitSearch: ->
      paramsDict =
        search_type: @get('search_type')
        raw_search_params: JSON.stringify(@get('query_params'))

      submitPromise = $.post(glados.Settings.CHEMBL_SUBMIT_SS_SEARCH_ENDPOINT, paramsDict)
      thisModel = @
      submitPromise.then (data) ->
        thisModel.set('search_id', data.search_id)
        thisModel.setState(glados.models.Search.StructureSearchModel.STATES.SEARCH_QUEUED)

#-------------------------------------------------------------------------------------------------------------------
# Connectivity search
#-------------------------------------------------------------------------------------------------------------------
    loadQueryForConnectivitySearch: ->
      queryParams = @get('query_params')
      searchTerm = queryParams.search_term

      @setState(glados.models.Search.StructureSearchModel.STATES.SEARCHING)
      @set('progress', 0)

      if !searchTerm.startsWith('CHEMBL')
        @loadQueryForConnectivitySearchFromSmiles(searchTerm)
      else
        esDocURL = "#{glados.Settings.ES_PROXY_ES_DOCS_BASE_URL}/#{Compound.prototype.indexName}/#{searchTerm}?source=molecule_structures.canonical_smiles"
        thisModel = @
        $.get(esDocURL).done((response) ->
          molData = response._source
          smiles = glados.Utils.getNestedValue(molData, 'molecule_structures.canonical_smiles',
            forceAsNumber = false, customNullValueLabel = undefined, returnUndefined = true)
          if not smiles?
            thisModel.set('error_message', "#{searchTerm} does not have a SMILES!")
            thisModel.setState(glados.models.Search.StructureSearchModel.STATES.ERROR_STATE)
          else
            thisModel.loadQueryForConnectivitySearchFromSmiles(smiles)
        ).fail((error) ->
          thisModel.set('error_message', error.responseText)
          thisModel.setState(glados.models.Search.StructureSearchModel.STATES.ERROR_STATE)
        )


    loadQueryForConnectivitySearchFromSmiles: (smiles) ->
      smiles2InchiKeyUrl = "#{glados.Settings.BEAKER_BASE_URL}smiles2inchiKey"
      smiles2InchiKeyPromise = $.post(smiles2InchiKeyUrl, smiles)

      thisModel = @
      smiles2InchiKeyPromise.then((response) ->
        connectivityLayer = response.split('-')[0]
        stickyQuery = {
          'query_string': {
            'query': "_metadata.hierarchy.family_inchi_connectivity_layer:#{connectivityLayer}"
          }

        }
        thisModel.set('connectivity_query', stickyQuery)
        thisModel.setState(glados.models.Search.StructureSearchModel.STATES.FINISHED)
      ).fail((error) ->
        thisModel.set('error_message', error.responseText)
        thisModel.setState(glados.models.Search.StructureSearchModel.STATES.ERROR_STATE)
      )



#-------------------------------------------------------------------------------------------------------------------
# Check search progress
#-------------------------------------------------------------------------------------------------------------------
    getProgressURL: ->
      search_id = @get('search_id')

      url = glados.Settings.DELAYED_JOB_STATUS_URL_GENERATOR
        job_id: encodeURIComponent(search_id)

      return url

    checkSearchStatusPeriodically: ->
      progressURL = @getProgressURL()
      thisModel = @
      getProgress = $.get(progressURL)

      getProgress.then (response) ->
        statusDescription = response.status_description

        if statusDescription? and statusDescription != 'None'
          parsedStatusDescription = JSON.parse(statusDescription)
          thisModel.set('status_description', parsedStatusDescription)

        status = response.status
        if status == 'ERROR'

          thisModel.set('error_message', response.msg)
          thisModel.setState(glados.models.Search.StructureSearchModel.STATES.ERROR_STATE)

        else if status == 'QUEUED'

          setTimeout(thisModel.checkSearchStatusPeriodically.bind(thisModel), 1000)

        else if status == 'RUNNING'

          thisModel.set('progress', response.progress)
          thisModel.setState(glados.models.Search.StructureSearchModel.STATES.SEARCHING)
          setTimeout(thisModel.checkSearchStatusPeriodically.bind(thisModel), 1000)

        else if status == 'FINISHED'

          contextUrl = "https://#{response.output_files_urls['results.json']}"
          thisModel.set('expires', response.expires_at)
          thisModel.setState(glados.models.Search.StructureSearchModel.STATES.LOADING_RESULTS)
          $.get(contextUrl).done((results) ->
            searchType = thisModel.get('search_type')
            if searchType == glados.models.Search.StructureSearchModel.SEARCH_TYPES.SEQUENCE.BLAST
              id_property = 'target_chembl_id'
            else
              id_property = 'molecule_chembl_id'

            ids = []
            search_results = results['search_results']
            for result in search_results
              ids.push(result[id_property])

            thisModel.set('ids_list', ids)
            thisModel.setState(glados.models.Search.StructureSearchModel.STATES.FINISHED)
          ).fail((error) ->
            thisModel.set('error_message', response.msg)
            thisModel.setState(glados.models.Search.StructureSearchModel.STATES.ERROR_STATE)
          )

        else
          setTimeout(thisModel.checkSearchStatusPeriodically.bind(thisModel), 1000)

    getContextObj: ->
      searchType = @get('search_type')

      if searchType == glados.models.Search.StructureSearchModel.SEARCH_TYPES.STRUCTURE.CONNECTIVITY
        return undefined

      if searchType == glados.models.Search.StructureSearchModel.SEARCH_TYPES.STRUCTURE.SIMILARITY
        queryParams = @.get('query_params')
        smiles = queryParams.search_term
        threshold = queryParams.threshold
        ws_base_url = glados.Settings.WS_BASE_URL
        if ws_base_url.endsWith('/')
          ws_base_url = ws_base_url.slice(0, -1)

        contextObj = {
          'context_type': 'DIRECT_SIMILARITY',
          'context_id': smiles,
          'web_services_base_url': ws_base_url,
          'similarity_threshold': threshold
        }
        return contextObj

      searchID = @get('search_id')

      contextObj = {
        'context_type': searchType,
        'context_id': searchID,
        'delayed_jobs_base_url': glados.Settings.DELAYED_JOBS_BASE_URL
      }

      return contextObj
#-------------------------------------------------------------------------------------------------------------------
# State handling
#-------------------------------------------------------------------------------------------------------------------
    getState: -> @get('state')
    setState: (newState) ->
      if newState == glados.models.Search.StructureSearchModel.STATES.SEARCH_QUEUED
        @checkSearchStatusPeriodically()
      else if newState == glados.models.Search.StructureSearchModel.STATES.FINISHED
        @trigger(glados.models.Search.StructureSearchModel.EVENTS.RESULTS_READY)

      @set('state', newState)

glados.models.Search.StructureSearchModel.STATES =
  INITIAL_STATE: 'INITIAL_STATE'
  ERROR_STATE: 'ERROR_STATE'
  SEARCH_QUEUED: 'SEARCH_QUEUED'
  SEARCHING: 'SEARCHING'
  LOADING_RESULTS: 'LOADING_RESULTS'
  FINISHED: 'FINISHED'

glados.models.Search.StructureSearchModel.EVENTS =
  RESULTS_READY: 'RESULTS_READY'

glados.models.Search.StructureSearchModel.SEARCH_TYPES =
  STRUCTURE:
    SIMILARITY: 'SIMILARITY'
    SUBSTRUCTURE: 'SUBSTRUCTURE'
    CONNECTIVITY: 'CONNECTIVITY'
  SEQUENCE:
    BLAST: 'BLAST'