glados.useNameSpace 'glados.models.Search',

  SearchByIDsModel: Backbone.Model.extend

    initialize: ->
      @setState(glados.models.Search.SearchByIDsModel.STATES.INITIAL_STATE)

#-------------------------------------------------------------------------------------------------------------------
# Check search progress
#-------------------------------------------------------------------------------------------------------------------
    getProgressURL: ->
      search_job_id = @get('search_job_id')

      url = glados.Settings.DELAYED_JOB_STATUS_URL_GENERATOR
        job_id: encodeURIComponent(search_job_id)

      return url

    checkSearchStatusPeriodically: ->
      progressURL = @getProgressURL()
      thisModel = @

      getProgress = $.get(progressURL)

      getProgress.then (response) ->
        status = response.status
        searchTo = JSON.parse(response.raw_params).to
        searchType = thisModel.getSearchTypeFromSearchTo(searchTo)
        thisModel.set('search_type', searchType)

        if status == 'FINISHED'

          thisModel.setState(glados.models.Search.SearchByIDsModel.STATES.FINISHED)

        else
          setTimeout(thisModel.checkSearchStatusPeriodically.bind(thisModel), 2000)


    getSearchTypeFromSearchTo: (searchTo) ->
      if searchTo == 'CHEMBL_COMPOUNDS'

        return glados.models.Search.SearchByIDsModel.SEARCH_DESTINATIONS.TO_COMPOUNDS

      else if searchTo == 'CHEMBL_TARGETS'

        return glados.models.Search.SearchByIDsModel.SEARCH_DESTINATIONS.TO_TARGETS

#-------------------------------------------------------------------------------------------------------------------
# State handling
#-------------------------------------------------------------------------------------------------------------------
    getState: -> @get('state')
    setState: (newState) ->
      if newState == glados.models.Search.SearchByIDsModel.STATES.FINISHED
        @trigger(glados.models.Search.SearchByIDsModel.EVENTS.RESULTS_READY)

      @set('state', newState)

    getContextObj: ->
      searchType = @get('search_type')
      searchID = @get('search_job_id')

      contextObj = {
        'context_type': searchType,
        'context_id': searchID,
        'delayed_jobs_base_url': glados.Settings.DELAYED_JOBS_BASE_URL
      }

      return contextObj

glados.models.Search.SearchByIDsModel.STATES =
  INITIAL_STATE: 'INITIAL_STATE'
  FINISHED: 'FINISHED'

glados.models.Search.SearchByIDsModel.EVENTS =
  RESULTS_READY: 'RESULTS_READY'

glados.models.Search.SearchByIDsModel.SEARCH_DESTINATIONS =
  TO_COMPOUNDS: 'TO_COMPOUNDS'
  TO_TARGETS: 'TO_TARGETS'