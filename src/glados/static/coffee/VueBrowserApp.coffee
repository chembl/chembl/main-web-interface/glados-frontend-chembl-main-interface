class VueBrowserApp

  @initVueBrowser = (entityName, encodedState) ->

    $vueBrowserIframe = $('#BCK-EntityBrowserVue')
    console.log('$vueBrowserIframe: ', $vueBrowserIframe)
    urlStatePart = if encodedState? then "#{encodedState}" else ""
    console.log('glados.Settings.EMBEDDED_VUE_BASE_URL: ', glados.Settings.EMBEDDED_VUE_BASE_URL)
    console.log('glados.Settings.EMBEDDED_VUE_BASE_URL: ', glados.Settings.EMBEDDED_VUE_BASE_URL?)
    console.log('glados.Settings: ', glados.Settings)
    iframeURL = "#{glados.Settings.EMBEDDED_VUE_BASE_URL}/browsers/compound/#{entityName}/#{urlStatePart}"
    console.log('iframeURL: ', iframeURL)
    glados.helpers.EmbeddedVueHelper.setIframeDynamicResizeListener(
      $vueBrowserIframe,
      'DrugWarningsBrowser'
    )

    glados.helpers.EmbeddedVueHelper.setIframeDatasetStateListener(
      $vueBrowserIframe,
      'DrugWarningsBrowser'
    )

    $vueBrowserIframe.attr('src', iframeURL)