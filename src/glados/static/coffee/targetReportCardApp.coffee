class TargetReportCardApp extends glados.ReportCardApp

  # -------------------------------------------------------------
  # Initialisation
  # -------------------------------------------------------------
  @init = ->
    super

    target = TargetReportCardApp.getCurrentTarget()

    breadcrumbLinks = [
      {
        label: target.get('id')
        link: Target.get_report_card_url(target.get('id'))
      }
    ]
    glados.apps.BreadcrumbApp.setBreadCrumb(breadcrumbLinks)

    TargetReportCardApp.initTargetNameAndClassification()
    TargetReportCardApp.initTargetComponents()
    TargetReportCardApp.initTargetRelations()
    TargetReportCardApp.initApprovedDrugsClinicalCandidates()
    TargetReportCardApp.initBioactivities()
    TargetReportCardApp.initLigandEfficiencies()
    TargetReportCardApp.initAssociatedCompounds()
    TargetReportCardApp.initGeneCrossReferences()
    TargetReportCardApp.initProteinCrossReferences()
    TargetReportCardApp.initDomainCrossReferences()
    TargetReportCardApp.initStructureCrossReferences()


    target.fetch()

  # -------------------------------------------------------------
  # Singleton
  # -------------------------------------------------------------
  @getCurrentTarget = ->
    if not @currentTarget?

      targetChemblID = glados.Utils.URLS.getCurrentModelChemblID()

      @currentTarget = new Target
        target_chembl_id: targetChemblID
        fetch_from_elastic: true
      return @currentTarget

    else return @currentTarget

  # -------------------------------------------------------------
  # Section initialisation
  # -------------------------------------------------------------
  @initTargetNameAndClassification = ->
    target = TargetReportCardApp.getCurrentTarget()

    viewConfig =
      embed_section_name: 'target_name_and_classification'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: target
      el: $('#TNameClassificationCard')
      config: viewConfig
      section_id: 'TargetNameAndClassification'
      section_label: 'Name And Classification'
      entity_name: Target.prototype.entityName
      report_card_app: @
      vue_component: 'TargetNameAndClassification'

  @initTargetComponents = ->
    target = TargetReportCardApp.getCurrentTarget()

    viewConfig =
      embed_section_name: 'target_components'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: target
      el: $('#TComponentsCard')
      config: viewConfig
      section_id: 'TargetComponents'
      section_label: 'Components'
      entity_name: Target.prototype.entityName
      report_card_app: @
      vue_component: 'TargetComponents'

  @initTargetRelations = ->
    target = TargetReportCardApp.getCurrentTarget()

    viewConfig =
      embed_section_name: 'target_relations'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    theView = new glados.views.ReportCards.EmbeddedVueInCardView
      model: target
      el: $('#TRelationsCard')
      resource_type: Target.prototype.entityName
      section_id: 'TargetRelations'
      section_label: 'Relations'
      entity_name: Target.prototype.entityName
      config: viewConfig
      report_card_app: @
      vue_component: 'TargetRelations'

    relationsUrl = glados.Settings.WS_BASE_URL + 'target_relation.json?related_target_chembl_id=' +
      glados.Utils.URLS.getCurrentModelChemblID()

    $.get(relationsUrl).done((response) ->
      totalCount = response.page_meta.total_count
      if totalCount == 0
        theView.hideSection()
        theView.forceHidden = true
    )


  #CHEMBL2363965
  @initApprovedDrugsClinicalCandidates = ->
    target = TargetReportCardApp.getCurrentTarget()

    viewConfig =
      embed_section_name: 'approved_drugs_and_clinical_candidates_info'
      embed_identifier: target.get('target_chembl_id')
      show_if: (model) ->
        if not model.attributes._metadata.generated_resources?
          return false
        return model.attributes._metadata.generated_resources.has_mechanisms == true
      properties_group: 'target_report_card_table'

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: target
      el: $('#ApprovedDrugsAndClinicalCandidatesCard')
      config: viewConfig
      section_id: 'ApprovedDrugsAndClinicalCandidates'
      section_label: 'Drugs And Clinical Candidates'
      entity_name: Target.prototype.entityName
      report_card_app: @
      vue_component: 'ApprovedDrugsAndClinicalCandidates'

    if GlobalVariables['EMBEDED']
      target.fetch()


  @initBioactivities = ->
    target = TargetReportCardApp.getCurrentTarget()

    viewConfig =
      embed_section_name: 'bioactivities'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: target
      el: $('#TActivityChartsCard')
      config: viewConfig
      section_id: 'ActivityCharts'
      section_label: 'Activity Charts'
      entity_name: Target.prototype.entityName
      report_card_app: @
      vue_component: 'TargetActivityCharts'

  @initLigandEfficiencies = ->
    target = TargetReportCardApp.getCurrentTarget()

    viewConfig =
      embed_section_name: 'ligand_efficiencies'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: target
      el: $('#TLigandEfficienciesCard')
      config: viewConfig
      section_id: 'TargetLigandEfficiencies'
      section_label: 'Ligand Efficiencies'
      entity_name: Target.prototype.entityName
      report_card_app: @
      vue_component: 'LigandEfficiencies'

  @initAssociatedCompounds = ->

    target = TargetReportCardApp.getCurrentTarget()

    viewConfig =
      embed_section_name: 'associated_compounds'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: target
      el: $('#TAssociatedCompoundProperties')
      config: viewConfig
      section_id: 'TargetAssociatedCompoundProperties'
      section_label: 'Associated Compounds'
      entity_name: Target.prototype.entityName
      report_card_app: @
      vue_component: 'TargetAssociatedCompounds'


  @initGeneCrossReferences = ->
    target = TargetReportCardApp.getCurrentTarget()

    viewConfig =
      embed_section_name: 'gene_cross_refs'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()
      show_if: (model) ->
        references = model.get('cross_references')
        if not references?
          return false

        if references.length == 0
          return false

        return true

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: target
      el: $('#TGeneCrossReferencesCard')
      config: viewConfig
      section_id: 'TargetCrossReferencesGene'
      section_label: 'Gene Cross References'
      entity_name: Target.prototype.entityName
      report_card_app: @
      vue_component: 'TargetGeneXRefs'

    if GlobalVariables['EMBEDED']
      target.fetch()

  @initProteinCrossReferences = ->
    target = TargetReportCardApp.getCurrentTarget()

    viewConfig =
      embed_section_name: 'protein_cross_refs'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()
      show_if: (model) ->
        references = model.get('cross_references')
        if not references?
          return false

        if references.length == 0
          return false

        return true

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: target
      el: $('#TProteinCrossReferencesCard')
      config: viewConfig
      section_id: 'TargetCrossReferencesProtein'
      section_label: 'Protein Cross References'
      entity_name: Target.prototype.entityName
      report_card_app: @
      vue_component: 'TargetProteinXRefs'

    if GlobalVariables['EMBEDED']
      target.fetch()

  @initDomainCrossReferences = ->
    target = TargetReportCardApp.getCurrentTarget()

    viewConfig =
      embed_section_name: 'domain_cross_refs'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()
      show_if: (model) ->
        references = model.get('cross_references')
        if not references?
          return false

        if references.length == 0
          return false

        return true

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: target
      el: $('#TDomainCrossReferencesCard')
      config: viewConfig
      section_id: 'TargetCrossReferencesDomain'
      section_label: 'Domain Cross References'
      entity_name: Target.prototype.entityName
      report_card_app: @
      vue_component: 'TargetDomainXRefs'

    if GlobalVariables['EMBEDED']
      target.fetch()

  @initStructureCrossReferences = ->
    target = TargetReportCardApp.getCurrentTarget()

    viewConfig =
      embed_section_name: 'structure_cross_refs'
      embed_identifier: glados.Utils.URLS.getCurrentModelChemblID()
      show_if: (model) ->
        references = model.get('cross_references')
        if not references?
          return false

        if references.length == 0
          return false

        return true

    new glados.views.ReportCards.EmbeddedVueInCardView
      model: target
      el: $('#TStructureCrossReferencesCard')
      config: viewConfig
      section_id: 'TargetCrossReferencesStructure'
      section_label: 'Structure Cross References'
      entity_name: Target.prototype.entityName
      report_card_app: @
      vue_component: 'TargetStructureXRefs'

    if GlobalVariables['EMBEDED']
      target.fetch()

  @initMiniBioactivitiesHistogram = ($containerElem, chemblID) ->
    bioactivities = TargetReportCardApp.getAssociatedBioactivitiesAgg(chemblID)

    stdTypeProp = glados.models.visualisation.PropertiesFactory.getPropertyConfigFor('Activity', 'STANDARD_TYPE',
      withColourScale = true)

    barsColourScale = stdTypeProp.colourScale

    config =
      max_categories: 8
      bars_colour_scale: barsColourScale
      fixed_bar_width: true
      hide_title: false
      x_axis_prop_name: 'types'
      properties:
        std_type: stdTypeProp
      initial_property_x: 'std_type'

    new glados.views.Visualisation.HistogramView
      model: bioactivities
      el: $containerElem
      config: config

    bioactivities.fetch()

  @initMiniCompoundsHistogram = ($containerElem, chemblID) ->
    associatedCompounds = TargetReportCardApp.getAssociatedCompoundsAgg(chemblID, minCols = 8,
      maxCols = 8, defaultCols = 8)

    config =
      max_categories: 8
      fixed_bar_width: true
      hide_title: false
      x_axis_prop_name: 'x_axis_agg'
      properties:
        mwt: glados.models.visualisation.PropertiesFactory.getPropertyConfigFor('Compound', 'FULL_MWT')
      initial_property_x: 'mwt'

    new glados.views.Visualisation.HistogramView
      model: associatedCompounds
      el: $containerElem
      config: config

    associatedCompounds.fetch()

  @initMiniHistogramFromFunctionLink = ->
    $clickedLink = $(@)

    [paramsList, constantParamsList, $containerElem] = \
      glados.views.PaginatedViews.PaginatedTable.prepareAndGetParamsFromFunctionLinkCell($clickedLink)

    histogramType = constantParamsList[0]
    targetChemblID = paramsList[0]
    if histogramType == 'activities'
      TargetReportCardApp.initMiniBioactivitiesHistogram($containerElem, targetChemblID)
    else if histogramType == 'compounds'
      TargetReportCardApp.initMiniCompoundsHistogram($containerElem, targetChemblID)

  # --------------------------------------------------------------------------------------------------------------------
  # Aggregations
  # --------------------------------------------------------------------------------------------------------------------
  @getAssociatedCompoundsAgg = (chemblID, minCols = 1, maxCols = 20, defaultCols = 10) ->
    queryConfig =
      type: glados.models.Aggregations.Aggregation.QueryTypes.MULTIMATCH
      queryValueField: 'target_chembl_id'
      fields: ['_metadata.related_targets.all_chembl_ids']

    aggsConfig =
      aggs:
        x_axis_agg:
          field: 'molecule_properties.full_mwt'
          type: glados.models.Aggregations.Aggregation.AggTypes.RANGE
          min_columns: minCols
          max_columns: maxCols
          num_columns: defaultCols
          bucket_links:
            bucket_filter_template: '_metadata.related_targets.all_chembl_ids:{{target_chembl_id}} ' +
              'AND molecule_properties.full_mwt:(>={{min_val}} AND <={{max_val}})'
            template_data:
              target_chembl_id: 'target_chembl_id'
              min_val: 'BUCKET.from'
              max_val: 'BUCKETS.to'
            link_generator: Compound.getCompoundsListURL

    associatedCompounds = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.COMPOUND_INDEX_URL
      query_config: queryConfig
      target_chembl_id: chemblID
      aggs_config: aggsConfig

    return associatedCompounds

  @getAssociatedBioactivitiesAgg = (chemblID) ->
    queryConfig =
      type: glados.models.Aggregations.Aggregation.QueryTypes.QUERY_STRING
      query_string_template: 'target_chembl_id:{{target_chembl_id}}'
      template_data:
        target_chembl_id: 'target_chembl_id'

    aggsConfig =
      aggs:
        types:
          type: glados.models.Aggregations.Aggregation.AggTypes.TERMS
          field: 'standard_type'
          size: 20
          bucket_links:
            bucket_filter_template: 'target_chembl_id:{{target_chembl_id}} ' +
              'AND standard_type:("{{bucket_key}}"' +
              '{{#each extra_buckets}} OR "{{this}}"{{/each}})'
            template_data:
              target_chembl_id: 'target_chembl_id'
              bucket_key: 'BUCKET.key'
              extra_buckets: 'EXTRA_BUCKETS.key'

            link_generator: Activity.getActivitiesListURL

    bioactivities = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.ACTIVITY_INDEX_URL
      query_config: queryConfig
      target_chembl_id: chemblID
      aggs_config: aggsConfig

    return bioactivities

  @getAssociatedAssaysAgg = (chemblID) ->
    queryConfig =
      type: glados.models.Aggregations.Aggregation.QueryTypes.QUERY_STRING
      query_string_template: 'target_chembl_id:{{target_chembl_id}}'
      template_data:
        target_chembl_id: 'target_chembl_id'

    aggsConfig =
      aggs:
        types:
          type: glados.models.Aggregations.Aggregation.AggTypes.TERMS
          field: '_metadata.assay_generated.type_label'
          size: 20
          bucket_links:
            bucket_filter_template: 'target_chembl_id:{{target_chembl_id}} ' +
              'AND _metadata.assay_generated.type_label:("{{bucket_key}}"' +
              '{{#each extra_buckets}} OR "{{this}}"{{/each}})'
            template_data:
              target_chembl_id: 'target_chembl_id'
              bucket_key: 'BUCKET.key'
              extra_buckets: 'EXTRA_BUCKETS.key'

            link_generator: Assay.getAssaysListURL

    associatedAssays = new glados.models.Aggregations.Aggregation
      index_url: glados.models.Aggregations.Aggregation.ASSAY_INDEX_URL
      query_config: queryConfig
      target_chembl_id: chemblID
      aggs_config: aggsConfig

    return associatedAssays