"""
Functions to register the structure search usage
"""
import requests
from django.conf import settings

from src.glados.usage_statistics import authentication


class StructureSearchStatisticsError(Exception):
    """
    Class for errors in this module
    """


def register_structure_search(search_type):
    token = authentication.get_es_proxy_special_access_token()

    headers = {'X-Special-Access-Key': token}

    payload = {
        "search_type": search_type,
        "time_taken": 0,
    }

    url = f'{settings.ES_PROXY_API_BASE_URL_INTERNAL}/usage_statistics/register_structure_search'
    request = requests.post(url, data=payload, headers=headers)

    status_code = request.status_code
    if status_code != 200:
        raise StructureSearchStatisticsError('There was an error while registering a structure search event!')
