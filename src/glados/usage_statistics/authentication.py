"""
Functions to handle the authentication to the es proxy special access endpoints
"""
import requests
from requests.auth import HTTPBasicAuth
from django.conf import settings
from django.core.cache import cache


def get_es_proxy_special_access_token():
    cache_key = 'ES_PROXY_SPECIAL_ACCESS_TOKEN'
    cached_token = cache.get(cache_key)
    if cached_token is not None:
        return cached_token

    login_url = f'{settings.ES_PROXY_API_BASE_URL_INTERNAL}/authorisation/login'
    special_access_username = settings.ES_PROXY_SPECIAL_ACCESS_USERNAME
    special_access_password = settings.ES_PROXY_SPECIAL_ACCESS_PASSWORD

    login_request = requests.get(login_url, auth=HTTPBasicAuth(special_access_username, special_access_password))
    login_response = login_request.json()

    token = login_response.get('token')
    cache.set(cache_key, cached_token, settings.ES_PROXY_SPECIAL_ACCESS_TOKEN_STORE_SECONDS)

    return token
