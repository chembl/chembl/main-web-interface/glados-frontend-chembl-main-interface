from django.urls import include, re_path
from django.conf.urls.static import static
from glados.utils_refactor import DirectTemplateView
from django.views.decorators.clickjacking import xframe_options_exempt
from django.conf import settings
from . import views
from glados import old_urls_redirector
from django.http import HttpResponse

from django.views.generic.base import RedirectView

common_urls = [

    # --------------------------------------------------------------------------------------------------------------------
    # Main Pages
    # --------------------------------------------------------------------------------------------------------------------
    re_path(r'^g/$', views.main_html_base_no_bar, name='javascript_routing'),

    re_path(r'^$',
            views.main_page, name='main'),

    re_path(r'^id_lookup/(?P<chembl_id>\w+)/$',
            views.id_lookup, name='id_lookup'),

    re_path(r'^entity/(?P<chembl_id>\w+)/$',
            views.entity, name='entity'),

    re_path(r'^tweets/$', views.get_latest_tweets_json, name='tweets'),

    re_path(r'^github_details/$', views.get_github_details, name='github_details'),

    re_path(r'^blog_entries/(?P<pageToken>.+)?$', views.get_latest_blog_entries, name='blog_entries'),

    re_path(r'^visualise/$', views.visualise, name='visualise'),

    re_path(r'^handlebars/visualisation_sources/$',
            xframe_options_exempt(
                DirectTemplateView.as_view(
                    template_name="glados/Handlebars/LazilyLoaded/Visualisations/VisualisationsSources.html")
            ), ),

    re_path(r'^design_components/$', views.design_components, name='design_components'),

    re_path(r'^marvin_search_fullscreen/$',
            DirectTemplateView.as_view(template_name="glados/marvin_search_fullscreen.html"), ),

    re_path(r'^compound_3D_speck/$',
            DirectTemplateView.as_view(template_name="glados/comp_3D_view_speck_fullscreen.html"), ),

    # ------------------------------------------------------------------------------------------------------------------
    # Redirections to other chembl resources and documentation
    # ------------------------------------------------------------------------------------------------------------------

    re_path(r'^acknowledgements/$',
            RedirectView.as_view(url='https://chembl.gitbook.io/chembl-interface-documentation/acknowledgments',
                                 permanent=True), name='acks'),

    re_path(r'^faq(s)?2/',
            RedirectView.as_view(
                url='https://chembl.gitbook.io/chembl-interface-documentation/frequently-asked-questions',
            ), name='faqs'),

    re_path(r'^training_materials/',
            RedirectView.as_view(
                url='https://chembl.gitbook.io/chembl-interface-documentation/training-material',
                permanent=True), name='training_material'),

    re_path(r'^faqs_browsing_related_entities/$',
            RedirectView.as_view(
                url='https://chembl.gitbook.io/chembl-interface-documentation/frequently-asked-questions/chembl-interface-questions#browsing-related-entities',
                permanent=True), name='faqs_browsing_related_entities'),

    re_path(r'^faqs_see_full_query/$',
            RedirectView.as_view(
                url='https://chembl.gitbook.io/chembl-interface-documentation/frequently-asked-questions/chembl-interface-questions#i-am-seeing-a-list-of-compounds-or-targets-assays-etc-how-can-i-see-the-query-used',
                permanent=True), name='faqs_see_full_query'),

    re_path(r'^faqs_edit_query_string/$',
            RedirectView.as_view(
                url='https://chembl.gitbook.io/chembl-interface-documentation/frequently-asked-questions/chembl-interface-questions#can-i-edit-the-query-being-used',
                permanent=True), name='faqs_edit_query_string'),

    re_path(r'^faqs_shorten_url/$',
            RedirectView.as_view(
                url='https://chembl.gitbook.io/chembl-interface-documentation/frequently-asked-questions/chembl-data-questions#are-my-queries-stored-and-if-so-are-they-routinely-deleted',
                permanent=True), name='faqs_shorten_url'),

    re_path(r'^contact_us/$',
            RedirectView.as_view(
                url='https://chembl.gitbook.io/chembl-interface-documentation/frequently-asked-questions/general-questions#how-do-i-report-errors-or-make-suggestions-for-the-interface',
                permanent=True), name='contact_us'),

    re_path(r'^about/$',
            RedirectView.as_view(url='https://chembl.gitbook.io/chembl-interface-documentation/about',
                                 permanent=True), name='about_us'),
    re_path(r'^downloads/$',
            RedirectView.as_view(url='https://chembl.gitbook.io/chembl-interface-documentation/downloads',
                                 permanent=True), name='downloads'),
    re_path(r'^chembl-ntd/$',
            RedirectView.as_view(url='https://chembl.gitbook.io/chembl-ntd/',
                                 permanent=True), name='chembl-ntd'),

    re_path(r'^status/$',
            RedirectView.as_view(url='http://chembl.github.io/status/',
                                 permanent=True), name='status'),

    re_path(r'^ws_home/$',
            RedirectView.as_view(url='https://chembl.gitbook.io/chembl-interface-documentation/web-services',
                                 permanent=True), name='web_services_home'),

    re_path(r'^ws/$',
            RedirectView.as_view(url='https://chembl.gitbook.io/chembl-interface-documentation/web-services',
                                 permanent=True), name='web_services_home_2'),

    re_path(r'^unichem_home/$',
            RedirectView.as_view(url='https://www.ebi.ac.uk/unichem',
                                 permanent=True), name='unichem_home'),

    re_path(r'^surechembl/$',
            RedirectView.as_view(url='https://www.surechembl.org',
                                 permanent=True), name='surechembl'),
    re_path(r'^maip/$',
            RedirectView.as_view(url='https://www.ebi.ac.uk/chembl/maip/',
                                 permanent=True), name='maip'),

    re_path(r'^chembl_rdf/$',
            RedirectView.as_view(url='https://www.ebi.ac.uk/rdf',
                                 permanent=True), name='chembl_rdf'),

    re_path(r'^chembl_blog/$',
            RedirectView.as_view(url='https://chembl.blogspot.co.uk',
                                 permanent=True), name='chembl_blog'),

    re_path(r'^chembl_twitter/$',
            RedirectView.as_view(url='https://twitter.com/chembl',
                                 permanent=True), name='chembl_twitter'),

    re_path(r'^db_schema',
            RedirectView.as_view(
                url=f'https://ftp.ebi.ac.uk/pub/databases/chembl/ChEMBLdb/latest/{settings.DOWNLOADS_RELEASE_NAME}'
                    f'_schema.png',
                permanent=True),
            name='db_schema'),

    re_path(r'^admesarfari/$',
            RedirectView.as_view(
                url="https://chembl.gitbook.io/chembl-interface-documentation/legacy-resources#adme-sarfari",
                permanent=True),
            name='adme_sarfari'),

    re_path(r'^sarfari/kinasesarfari/$',
            RedirectView.as_view(
                url="https://chembl.gitbook.io/chembl-interface-documentation/legacy-resources#kinase-sarfari",
                permanent=True),
            name='kinase_sarfari'),

    re_path(r'^sarfari/GPCRsarfari/$',
            RedirectView.as_view(
                url="https://chembl.gitbook.io/chembl-interface-documentation/legacy-resources#gpcr-sarfari",
                permanent=True),
            name='gpcr_sarfari'),

    re_path(r'^malaria/$',
            RedirectView.as_view(
                url="https://chembl.gitbook.io/chembl-interface-documentation/legacy-resources#chembl-malaria",
                permanent=True),
            name='chembl_malaria'),

    re_path(r'^download_expiration/$',
            RedirectView.as_view(
                url="https://chembl.gitbook.io/chembl-interface-documentation/frequently-asked-questions/chembl-download-questions#what-is-the-expiration-date-in-the-downloads-from-the-interface",
                permanent=True),
            name='download_expiration'),

    re_path(r'^sssearch_expiration/$',
            RedirectView.as_view(
                url="https://chembl.gitbook.io/chembl-interface-documentation/frequently-asked-questions/chembl-interface-questions#what-is-the-expiration-date-in-the-structure-based-searches-from-the-interface",
                permanent=True),
            name='sssearch_expiration'),

    # --------------------------------------------------------------------------------------------------------------------
    # Tests
    # --------------------------------------------------------------------------------------------------------------------
    re_path(r'^layout_test/$', DirectTemplateView.as_view(template_name="glados/layoutTest.html"), ),
    re_path(r'^js_tests/$', DirectTemplateView.as_view(template_name="glados/jsTests.html"), ),

    # --------------------------------------------------------------------------------------------------------------------
    # Embedding
    # --------------------------------------------------------------------------------------------------------------------
    re_path(r'^embed/$',
            xframe_options_exempt(DirectTemplateView.as_view(template_name="glados/Embedding/embed_base.html")), ),

    re_path(r'^embed/tiny/(?P<url_hash>.*?)$', views.render_params_from_hash_when_embedded, name='embed-tiny'),
    # --------------------------------------------------------------------------------------------------------------------
    # Compounds
    # --------------------------------------------------------------------------------------------------------------------

    re_path(r'^compound_report_card/(?P<chembl_id>\w+)/$', views.compound_report_card,
            name='compound_report_card'),

    re_path(r'^compound_metabolism/(?P<chembl_id>\w+)$', xframe_options_exempt(
        DirectTemplateView.as_view(
            template_name="glados/MoleculeMetabolismGraphFS.html")), ),

    # --------------------------------------------------------------------------------------------------------------------
    # Targets
    # --------------------------------------------------------------------------------------------------------------------

    re_path(r'^target_report_card/(?P<chembl_id>\w+)/$',
            views.target_report_card, name='target_report_card'),

    # --------------------------------------------------------------------------------------------------------------------
    # Assays
    # --------------------------------------------------------------------------------------------------------------------

    re_path(r'^assay_report_card/(?P<chembl_id>\w+)/$',
            views.assay_report_card, name='assay_report_card'),

    # --------------------------------------------------------------------------------------------------------------------
    # Documents
    # --------------------------------------------------------------------------------------------------------------------

    re_path(r'^document_report_card/(?P<chembl_id>\w+)/$',
            views.document_report_card, name='document_report_card'),

    re_path(r'^document_assay_network/(?P<chembl_id>\w+)/$',
            DirectTemplateView.as_view(template_name="glados/DocumentAssayNetwork/DocumentAssayNetwork.html"), ),

    re_path(r'^documents_with_same_terms/(?P<doc_terms>.+)/$',
            DirectTemplateView.as_view(template_name="glados/DocumentTerms/DocumentTermsSearch.html"), ),

    # --------------------------------------------------------------------------------------------------------------------
    # Cells
    # --------------------------------------------------------------------------------------------------------------------

    re_path(r'^cell_line_report_card/(?P<chembl_id>\w+)/$',
            views.cell_line_report_card, name='cell_line_report_card'),

    # --------------------------------------------------------------------------------------------------------------------
    # Tissues
    # --------------------------------------------------------------------------------------------------------------------
    re_path(r'^tissue_report_card/(?P<chembl_id>\w+)/$',
            views.tissue_report_card, name='tissue_report_card'),

    # ------------------------------------------------------------------------------------------------------------------
    # Old Interface redirections
    # ------------------------------------------------------------------------------------------------------------------
    re_path(r'^(index\.php\/)?(?P<entity_name>\w+)/inspect/(?P<chembl_id>\w+)/$',
            old_urls_redirector.redirect_report_card),

    # --------------------------------------------------------------------------------------------------------------------
    # Tiny urls
    # --------------------------------------------------------------------------------------------------------------------
    re_path(r'^g/tiny/(?P<url_hash>.*?)$', views.render_params_from_hash, name='tiny'),

    re_path(r'^robots.txt', lambda x: HttpResponse(
        "User-Agent: *\nDisallow: / \nUser-Agent: Twitterbot\nAllow: {0}img".format(settings.STATIC_URL),
        content_type="text/plain"),
            name="robots_file"),

    # --------------------------------------------------------------------------------------------------------------------
    # Tracking
    # --------------------------------------------------------------------------------------------------------------------
    re_path(r'^register_usage', views.register_usage, name='register_usage'),
    re_path(r'^register_structure_search', views.register_structure_search, name='register_structure_search'),

    # --------------------------------------------------------------------------------------------------------------------
    # Chembl UI API
    # --------------------------------------------------------------------------------------------------------------------
    re_path(r'^glados_api/', include('glados.api.urls')),

]

# ----------------------------------------------------------------------------------------------------------------------
# SERVER BASE PATH DEFINITION
# ----------------------------------------------------------------------------------------------------------------------

# add slash to the end of base path to avoid routing issues
BASE_PATH_TO_APPEND = settings.SERVER_BASE_PATH
if BASE_PATH_TO_APPEND != '' and BASE_PATH_TO_APPEND[-1] != '/':
    BASE_PATH_TO_APPEND = f'{BASE_PATH_TO_APPEND}/'

urlpatterns = [re_path(r'^' + settings.SERVER_BASE_PATH, include(common_urls))]

# ----------------------------------------------------------------------------------------------------------------------
# Static Files
# ----------------------------------------------------------------------------------------------------------------------

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
