# Environment cheats

```shell
python3 -m venv .venv
source .venv/bin/activate
pip3 install -r requirements.txt
````

# Running dev server

```shell
python3 manage.py runserver 
```
